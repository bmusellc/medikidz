//
//  WelcomeView.m
//  MediKidz
//
//  Created by Alexey Smirnov on 6/24/16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "WelcomeView.h"
#import "UILabel+TextMetrics.h"

#define kTitleText              @"Welcome to the Study!"
#define kSubText                @"We will explain what you can do with this app and allow you to provide your consent for using it"
#define kVerticalIndent         roundf(CGRectGetHeight(self.bounds) / 14.0f)
#define kHorizontalIndent       15.0f
#define kStartButtonSize        CGSizeMake(190, 50)

@interface WelcomeView ()

@property (strong, nonatomic)   UIImageView     *logoImageView;
@property (strong, nonatomic)   UIImageView     *characterView;
@property (strong, nonatomic)   UILabel         *titleLabel;
@property (strong, nonatomic)   UILabel         *textLabel;
@property (strong, nonatomic)   UIButton        *startButton;

@end

@implementation WelcomeView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.logoImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sanofiLogo"]];
        self.logoImageView.contentMode = UIViewContentModeScaleAspectFit;
        self.logoImageView.layer.masksToBounds = NO;
        self.logoImageView.layer.shadowColor = [UIColor blackColor].CGColor;
        self.logoImageView.layer.shadowOffset = CGSizeMake(0.0f, 3.0f);
        self.logoImageView.layer.shadowOpacity = 0.5f;
        [self addSubview:self.logoImageView];
        
        self.characterView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"character2"]];
        self.characterView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:self.characterView];

        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.titleLabel.font = [UIFont systemFontOfSize:34.0f];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.numberOfLines = 0;
        self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.titleLabel.textColor = self.tintColor;
        self.titleLabel.text = kTitleText;
        [self addSubview:self.titleLabel];
        
        self.textLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:20.0f];
        self.textLabel.textAlignment = NSTextAlignmentCenter;
        self.textLabel.numberOfLines = 0;
        self.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.textLabel.textColor = [UIColor blackColor];
        self.textLabel.text = kSubText;
        [self addSubview:self.textLabel];
        
        self.startButton = [UIButton buttonWithType:UIButtonTypeSystem];
        self.startButton.layer.borderColor = self.startButton.tintColor.CGColor;
        self.startButton.layer.borderWidth = 1.0f;
        self.startButton.layer.cornerRadius = 10.0f;
        [self.startButton setTitle:@"Get Started" forState:UIControlStateNormal];
        [self.startButton addTarget:self
                             action:@selector(_handleStartButtonTap:)
                   forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.startButton];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect logoRect = CGRectZero;
    CGSize logoSize = self.logoImageView.image.size;
    logoRect.size.width = CGRectGetWidth(self.bounds);
    logoRect.size.height = (CGRectGetWidth(self.bounds) / logoSize.width) * logoSize.height;
    self.logoImageView.frame = logoRect;
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.logoImageView.bounds];
    self.logoImageView.layer.shadowPath = shadowPath.CGPath;
    
    CGRect characterRect = CGRectZero;
    characterRect.size = self.characterView.image.size;
    characterRect.origin.x = roundf((CGRectGetWidth(self.bounds) - CGRectGetWidth(characterRect)) / 2.0f);
    characterRect.origin.y = CGRectGetMaxY(logoRect) + kVerticalIndent;
    self.characterView.frame = characterRect;
    
    CGRect titleRect = CGRectZero;
    titleRect.size = [self.titleLabel sizeThatReallyFits:CGSizeMake(CGRectGetWidth(self.bounds) - 2 * kHorizontalIndent, CGFLOAT_MAX)];
    titleRect.origin.x = roundf((CGRectGetWidth(self.bounds) - CGRectGetWidth(titleRect)) / 2.0f);
    titleRect.origin.y = CGRectGetMaxY(characterRect) + kVerticalIndent;
    self.titleLabel.frame = titleRect;
    
    CGRect textRect = CGRectZero;
    textRect.size = [self.textLabel sizeThatReallyFits:CGSizeMake(CGRectGetWidth(self.bounds) - 2 * kHorizontalIndent, CGFLOAT_MAX)];
    textRect.origin.x = roundf((CGRectGetWidth(self.bounds) - CGRectGetWidth(textRect)) / 2.0f);
    textRect.origin.y = CGRectGetMaxY(titleRect);
    self.textLabel.frame = textRect;
    
    CGRect startButtonRect = CGRectZero;
    startButtonRect.size = kStartButtonSize;
    startButtonRect.origin.x = roundf((CGRectGetWidth(self.bounds) - CGRectGetWidth(startButtonRect)) / 2.0f);
    startButtonRect.origin.y = CGRectGetMaxY(textRect) + kVerticalIndent;
    self.startButton.frame = startButtonRect;
}

#pragma mark - Private

- (void)_handleStartButtonTap:(UIButton *)okButton
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(welcomeViewDidTapOnStartButton:)])
    {
        [self.delegate welcomeViewDidTapOnStartButton:self];
    }
}

@end