//
//  PreSignInView.h
//  MediKidz
//
//  Created by Alexey Smirnov on 6/24/16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PreSignInView;

@protocol PreSignInViewDelegate <NSObject>

- (void)preSignInViewDidTapOnSignButton:(PreSignInView *)sender;

@end


@interface PreSignInView : UIView

@property (weak, nonatomic) id<PreSignInViewDelegate> delegate;

@end
