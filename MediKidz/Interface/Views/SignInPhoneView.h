//
//  SignInPhoneView.h
//  MediKidz
//
//  Created by Mike Ponomaryov on 24.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignInPhoneView : UIView

@property (nonatomic, strong)   UITextField *phoneTextField;
@property (nonatomic, strong)   UIButton    *signInButton;
@property (nonatomic, strong)   UIButton    *helpButton;

@end
