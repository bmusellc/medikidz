//
//  LevelUpPopover.m
//  MediKidz
//
//  Created by Stanislav Kovalchuk on 6/24/16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "LevelUpPopover.h"

@interface LevelUpPopover()

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *subtitleLabel;


@end

@implementation LevelUpPopover

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.userInteractionEnabled = YES;
    self.exclusiveTouch = YES;
    self.titleLabel.text = @"THIS IS YOUR\nFIRST ENERGY BOOST!";
    self.subtitleLabel.text = @"Level UP";
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (IBAction)handleCloseButtonTap:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(levelUpPopoverShouldClose:)])
    {
        [self.delegate levelUpPopoverShouldClose:self];
    }
}

@end
