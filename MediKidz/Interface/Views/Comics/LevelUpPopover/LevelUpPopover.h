//
//  LevelUpPopover.h
//  MediKidz
//
//  Created by Stanislav Kovalchuk on 6/24/16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LevelUpPopover;

@protocol LevelUpPopoverDelegate

- (void)levelUpPopoverShouldClose:(LevelUpPopover *)sender;

@end

@interface LevelUpPopover : UIView

@property (nonatomic, assign) NSObject<LevelUpPopoverDelegate>* delegate;

@end
