//
//  IntroView.m
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "IntroView.h"

#define kLogoTopMargin              50.0f
#define kCharactersTopMargin        140.0f
#define kAdultButtonBottomMargin    50.0f
#define kButtonsMargin              14.0f

@interface IntroView ()

@property (strong, nonatomic)   UIImageView    *logo;
@property (strong, nonatomic)   UIImageView    *characters;

@end

@implementation IntroView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"medikidzLogo"]];
        [self addSubview:self.logo];
        
        self.characters = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"characters"]];
        [self addSubview:self.characters];
        
        self.adultButton = [UIButton new];
        [self.adultButton setBackgroundImage:[UIImage imageNamed:@"adultButtonBg"] forState:UIControlStateNormal];
        [self.adultButton sizeToFit];
        self.adultButton.titleLabel.font = [UIFont fontWithName:@"FjallaOne" size:22.0f];
        [self.adultButton setTitleColor:RGBColor(202.0f, 28.0f, 31.0f) forState:UIControlStateNormal];
        [self.adultButton setTitle:@"I'm An Adult" forState:UIControlStateNormal];
        [self addSubview:self.adultButton];
        
        self.kidButton = [UIButton new];
        [self.kidButton setBackgroundImage:[UIImage imageNamed:@"kidButtonBg"] forState:UIControlStateNormal];
        [self.kidButton sizeToFit];
        self.kidButton.titleLabel.font = [UIFont fontWithName:@"FjallaOne" size:22.0f];;
        [self.kidButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.kidButton setTitle:@"I'm A Kid" forState:UIControlStateNormal];
        [self addSubview:self.kidButton];
    }
    return self;
}

- (void)layoutSubviews
{
    self.logo.center = CGPointMake(self.center.x, kLogoTopMargin + CGRectGetHeight(self.logo.frame) / 2.0f);
    self.characters.center = CGPointMake(self.center.x, kCharactersTopMargin + CGRectGetHeight(self.characters.frame) / 2.0f);
    self.adultButton.center = CGPointMake(self.center.x, CGRectGetHeight(self.frame) - CGRectGetHeight(self.logo.frame) / 2.0f - kAdultButtonBottomMargin);
    self.kidButton.center = CGPointMake(self.center.x, CGRectGetHeight(self.frame) - CGRectGetHeight(self.logo.frame) / 2.0f - kAdultButtonBottomMargin - CGRectGetHeight(self.adultButton.frame) - kButtonsMargin);
}

@end
