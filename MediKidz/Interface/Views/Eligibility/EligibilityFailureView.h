//
//  EligibilityFailureView.h
//  MediKidz
//
//  Created by Alexey Smirnov on 6/24/16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EligibilityFailureView;

@protocol EligibilityFailureViewDelegate <NSObject>

- (void)failureViewDidTapOnOkButton:(EligibilityFailureView *)sender;

@end

@interface EligibilityFailureView : UIView

@property (weak, nonatomic) id<EligibilityFailureViewDelegate> delegate;

@end
