//
//  EligibilityQuizFooter.m
//  MediKidz
//
//  Created by Stanislav Kovalchuk on 6/24/16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "EligibilityQuizFooter.h"
#import "CountryPicker.h"

@interface EligibilityQuizFooter ()<UIPickerViewDelegate>

@property (strong, nonatomic) IBOutlet CountryPicker *picker;
@end

@implementation EligibilityQuizFooter

+ (NSString *)reuseID
{
    return NSStringFromClass([self class]);
}

- (void)awakeFromNib
{
    [super awakeFromNib];

    [self.picker selectRow:221 inComponent:0 animated:NO];
    [self.picker setSelectedCountryCode:@"US" animated:NO];
}

- (NSString *)currentCountry
{
    return [self.picker selectedCountryName];
}

@end
