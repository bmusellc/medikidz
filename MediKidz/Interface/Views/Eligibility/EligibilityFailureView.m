//
//  EligibilityFailureView.m
//  MediKidz
//
//  Created by Alexey Smirnov on 6/24/16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "EligibilityFailureView.h"
#import "UILabel+TextMetrics.h"

#define kTextFailure                @"We appreciate your interest! Unfortunately, you and your child are not eligible to use this app."

#define kOkButtonSize               CGSizeMake(190, 50)
#define kImageCenterOffset          20.0f
#define kVerticalIndent             roundf(CGRectGetHeight(self.bounds) / 10.0f)
#define kHorizontalMargin           roundf(CGRectGetWidth(self.bounds) / 6)

@interface EligibilityFailureView ()

@property (strong, nonatomic)   UIImageView     *imageView;
@property (strong, nonatomic)   UILabel         *textLabel;
@property (strong, nonatomic)   UIButton        *okButton;

@end

@implementation EligibilityFailureView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"characterRobot"]];
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:self.imageView];
        
        self.textLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.textLabel.font = [UIFont systemFontOfSize:20.0f];
        self.textLabel.textAlignment = NSTextAlignmentCenter;
        self.textLabel.numberOfLines = 0;
        self.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.textLabel.textColor = [UIColor blackColor];
        self.textLabel.text = kTextFailure;
        [self addSubview:self.textLabel];
        
        self.okButton = [UIButton buttonWithType:UIButtonTypeSystem];
        self.okButton.layer.borderColor = self.okButton.tintColor.CGColor;
        self.okButton.layer.borderWidth = 1.0f;
        self.okButton.layer.cornerRadius = 10.0f;
        [self.okButton setTitle:@"Okay" forState:UIControlStateNormal];
        [self.okButton addTarget:self
                            action:@selector(_handleOkButtonTap:)
                  forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.okButton];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];

    CGRect imageRect = CGRectZero;
    imageRect.size = self.imageView.image.size;
    imageRect.origin.x = roundf((CGRectGetWidth(self.bounds) - CGRectGetWidth(imageRect)) / 2.0f);
    imageRect.origin.y = roundf(CGRectGetHeight(self.bounds) / 2.0f) - CGRectGetHeight(imageRect) + kImageCenterOffset;
    self.imageView.frame = imageRect;
    
    CGRect textRect = CGRectZero;
    textRect.size = [self.textLabel sizeThatReallyFits:CGSizeMake(CGRectGetWidth(self.bounds) - 2 * kHorizontalMargin, CGFLOAT_MAX)];
    textRect.origin.x = roundf((CGRectGetWidth(self.bounds) - CGRectGetWidth(textRect)) / 2.0f);
    textRect.origin.y = CGRectGetMaxY(imageRect) + kVerticalIndent;
    self.textLabel.frame = textRect;
    
    CGRect okButtonRect = CGRectZero;
    okButtonRect.size = kOkButtonSize;
    okButtonRect.origin.x = roundf((CGRectGetWidth(self.bounds) - CGRectGetWidth(okButtonRect)) / 2.0f);
    okButtonRect.origin.y = CGRectGetMaxY(textRect) + kVerticalIndent;
    self.okButton.frame = okButtonRect;
}

#pragma mark - Private

- (void)_handleOkButtonTap:(UIButton *)okButton
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(failureViewDidTapOnOkButton:)])
    {
        [self.delegate failureViewDidTapOnOkButton:self];
    }
}

@end
