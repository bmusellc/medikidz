//
//  EligibilityQuizCellCollectionViewCell.m
//  MediKidz
//
//  Created by Stanislav Kovalchuk on 6/24/16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "EligibilityQuizCellCollectionViewCell.h"

@interface EligibilityQuizCellCollectionViewCell()

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIButton *noButton;
@property (strong, nonatomic) IBOutlet UIButton *yesButton;


@end

@implementation EligibilityQuizCellCollectionViewCell

+ (NSString *)reuseID
{
    return NSStringFromClass([self class]);
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
    self.titleLabel.numberOfLines = 0;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.titleLabel setPreferredMaxLayoutWidth:CGRectGetWidth(self.bounds) - 20];
}

- (NSString *)title
{
    return self.titleLabel.text;
}

- (void)setTitle:(NSString *)title
{
    self.titleLabel.text = title;
}

- (BOOL)isChoiceMade
{
    return self.yesButton.selected || self.noButton.selected;
}

- (BOOL)yesChoosen
{
    return self.yesButton.selected == YES;
}

- (IBAction)handleNoButtonTap:(UIButton *)sender
{
    self.noButton.selected = YES;
    self.yesButton.selected = NO;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(eligibilityQuizCellCollectionViewCellMadeChoice)])
    {
        [self.delegate eligibilityQuizCellCollectionViewCellMadeChoice];
    }
}

- (IBAction)handleYesButtonTap:(UIButton *)sender
{
    self.yesButton.selected = YES;
    self.noButton.selected = NO;

    if (self.delegate && [self.delegate respondsToSelector:@selector(eligibilityQuizCellCollectionViewCellMadeChoice)])
    {
        [self.delegate eligibilityQuizCellCollectionViewCellMadeChoice];
    }
}

@end
