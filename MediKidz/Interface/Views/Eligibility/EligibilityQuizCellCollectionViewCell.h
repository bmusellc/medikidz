//
//  EligibilityQuizCellCollectionViewCell.h
//  MediKidz
//
//  Created by Stanislav Kovalchuk on 6/24/16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EligibilityQuizCellCollectionViewCellDelegate

- (void)eligibilityQuizCellCollectionViewCellMadeChoice;

@end

@interface EligibilityQuizCellCollectionViewCell : UICollectionViewCell

@property (nonatomic, assign)NSObject<EligibilityQuizCellCollectionViewCellDelegate>*   delegate;
@property (nonatomic, readonly) BOOL isChoiceMade;
@property (nonatomic, readonly) BOOL yesChoosen;
@property (nonatomic, assign) NSString* title;
+ (NSString *)reuseID;

@end
