//
//  EligibilityQuizFooter.h
//  MediKidz
//
//  Created by Stanislav Kovalchuk on 6/24/16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EligibilityQuizFooter : UICollectionReusableView

@property (nonatomic, readonly) NSString* currentCountry;
+ (NSString *)reuseID;

@end
