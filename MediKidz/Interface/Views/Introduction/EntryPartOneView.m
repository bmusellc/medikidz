//
//  EntryPartOneView.m
//  MediKidz
//
//  Created by Alexey Smirnov on 6/22/16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "EntryPartOneView.h"

#define kDistanceBetweenCharacters        roundf(CGRectGetWidth(self.bounds) / 10.0f)
#define kBottomIndent                     roundf(CGRectGetHeight(self.bounds) / 10.0f)
#define kBubbleOffset(characterHeight)    roundf(characterHeight / 8.0f)
#define kBubbleOneActiveSize(size)        CGSizeMake(0.92 * size.width, size.height)
#define kBubbleTwoActiveSize(size)        CGSizeMake(0.9 * size.width, size.height)
#define kRelativeContentSize(size)        CGSizeMake(0.8 * size.width, 0.7 * size.height)

@interface EntryPartOneView ()

/* Temporary commented
@property (strong, nonatomic) UIImageView *characterOneView;
@property (strong, nonatomic) UIImageView *characterTwoView;
@property (strong, nonatomic) UIImageView *bubbleOneView;
@property (strong, nonatomic) UIImageView *bubbleTwoView;

@property (strong, nonatomic) UILabel     *speechOneLabel;
@property (strong, nonatomic) UILabel     *speechTwoLabel;
 */

@property (strong, nonatomic) UIImageView   *sceneView;

@end

@implementation EntryPartOneView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
/* Temporary commented
        self.characterOneView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"character1"]];
        [self addSubview:self.characterOneView];
        self.characterTwoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"character2"]];
        [self addSubview:self.characterTwoView];
        self.bubbleOneView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bubbleCharacter1"]];
        [self addSubview:self.bubbleOneView];
        self.bubbleTwoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bubbleCharacter2"]];
        [self addSubview:self.bubbleTwoView];
        
        self.speechOneLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.speechOneLabel.numberOfLines = 0;
        self.speechOneLabel.font = [UIFont fontWithName:@"CCKissAndTell" size:12.0f];
        self.speechOneLabel.textColor = [UIColor blackColor];
        self.speechOneLabel.textAlignment = NSTextAlignmentCenter;
        self.speechOneLabel.text = @"HI! WE'RE THE MEDIKIDZ!";
        [self.bubbleOneView addSubview:self.speechOneLabel];
        
        self.speechTwoLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.speechTwoLabel.numberOfLines = 0;
        self.speechTwoLabel.font = [UIFont fontWithName:@"CCKissAndTell" size:12.0f];
        self.speechTwoLabel.textColor = [UIColor blackColor];
        self.speechTwoLabel.textAlignment = NSTextAlignmentCenter;
        self.speechTwoLabel.text = @"WE LIVE ON MEDILAND - SHAPED LIKE THE HUMAN BODY. IT'S OUR MISSION TO HELP YOU UNDERSTAND HEALTH AND ILLNESS.";
        [self.bubbleTwoView addSubview:self.speechTwoLabel];
 */
        self.sceneView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"screen1comics"]];
        self.sceneView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:self.sceneView];
        
        self.transform = CGAffineTransformMakeRotation(M_PI_2);
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.sceneView.frame = self.bounds;
/* Temporary commented
    CGRect characterOneRect = CGRectZero;
    characterOneRect.size = self.characterOneView.image.size;
    characterOneRect.origin.x = roundf((CGRectGetWidth(self.bounds) - kDistanceBetweenCharacters) / 2.0f) - CGRectGetWidth(characterOneRect);
    characterOneRect.origin.y = CGRectGetHeight(self.bounds) - kBottomIndent - CGRectGetHeight(characterOneRect);
    self.characterOneView.frame = characterOneRect;
    
    CGRect characterTwoRect = CGRectZero;
    characterTwoRect.size = self.characterTwoView.image.size;
    characterTwoRect.origin.x = roundf((CGRectGetWidth(self.bounds) + kDistanceBetweenCharacters) / 2.0f);
    characterTwoRect.origin.y = CGRectGetMaxY(characterOneRect) - CGRectGetHeight(characterTwoRect);
    self.characterTwoView.frame = characterTwoRect;
    
    CGRect bubbleOneRect = CGRectZero;
    bubbleOneRect.size = self.bubbleOneView.image.size;
    bubbleOneRect.origin.x = CGRectGetMinX(characterOneRect) - CGRectGetWidth(bubbleOneRect);
    bubbleOneRect.origin.y = CGRectGetMinY(characterOneRect) + kBubbleOffset(CGRectGetHeight(characterOneRect)) - CGRectGetHeight(bubbleOneRect);
    self.bubbleOneView.frame = bubbleOneRect;
    
    CGRect speechOneRect = CGRectZero;
    CGSize bubbleOneSize = kBubbleOneActiveSize(bubbleOneRect.size);
    speechOneRect.size = kRelativeContentSize(bubbleOneSize);
    speechOneRect.origin.x = roundf((bubbleOneSize.width - CGRectGetWidth(speechOneRect)) / 2.0f);
    speechOneRect.origin.y = roundf((bubbleOneSize.height - CGRectGetHeight(speechOneRect)) / 2.0f);
    self.speechOneLabel.frame = speechOneRect;
    
    CGRect bubbleTwoRect = CGRectZero;
    bubbleTwoRect.size = self.bubbleTwoView.image.size;
    bubbleTwoRect.origin.x = CGRectGetMaxX(characterTwoRect);
    bubbleTwoRect.origin.y = CGRectGetMinY(characterTwoRect) + kBubbleOffset(CGRectGetHeight(characterTwoRect)) - CGRectGetHeight(bubbleTwoRect);
    self.bubbleTwoView.frame = bubbleTwoRect;
    
    CGRect speechTwoRect = CGRectZero;
    CGSize bubbleTwoSize = kBubbleTwoActiveSize(bubbleTwoRect.size);
    speechTwoRect.size = kRelativeContentSize(bubbleTwoSize);
    speechTwoRect.origin.x = roundf((bubbleTwoSize.width - CGRectGetWidth(speechTwoRect)) / 2.0f) + (CGRectGetWidth(bubbleTwoRect) - bubbleTwoSize.width);
    speechTwoRect.origin.y = roundf((bubbleTwoSize.height - CGRectGetHeight(speechTwoRect)) / 2.0f);
    self.speechTwoLabel.frame = speechTwoRect;
 */
}

@end
