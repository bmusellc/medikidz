//
//  EntryPartTwoView.m
//  MediKidz
//
//  Created by Alexey Smirnov on 6/22/16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "EntryPartTwoView.h"

#define kSideIndent                       roundf(CGRectGetWidth(self.bounds) / 15.0f)
#define kBottomIndent                     roundf(CGRectGetHeight(self.bounds) / 20.0f)
#define kRelativeContentSize(size)        CGSizeMake(0.8 * size.width, 0.7 * size.height)
#define kAllowedIntersection              0.15
#define kBubbleOffset(characterHeight)    roundf(characterHeight / 8.0f)

@interface EntryPartTwoView ()

/* Temporary commented
@property (strong, nonatomic) UIImageView *characterOneView;
@property (strong, nonatomic) UIImageView *characterTwoView;
@property (strong, nonatomic) UIImageView *characterThreeView;
@property (strong, nonatomic) UIImageView *bubbleOneView;
@property (strong, nonatomic) UIImageView *bubbleTwoView;
@property (strong, nonatomic) UIImageView *bubbleThreeView;

@property (strong, nonatomic) UILabel     *speechOneLabel;
@property (strong, nonatomic) UILabel     *speechTwoLabel;
@property (strong, nonatomic) UILabel     *speechThreeLabel;
 */

@property (strong, nonatomic) UIImageView   *sceneView;

@end

@implementation EntryPartTwoView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
/* Temporary commented
        self.characterOneView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"character3"]];
        [self addSubview:self.characterOneView];
        self.characterTwoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"character4"]];
        [self addSubview:self.characterTwoView];
        self.characterThreeView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"character5"]];
        [self addSubview:self.characterThreeView];
        self.bubbleOneView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bubbleCharacter3"]];
        [self addSubview:self.bubbleOneView];
        self.bubbleTwoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bubbleCharacter4"]];
        [self addSubview:self.bubbleTwoView];
        self.bubbleThreeView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bubbleCharacter5"]];
        [self addSubview:self.bubbleThreeView];
        
        self.speechOneLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.speechOneLabel.numberOfLines = 0;
        self.speechOneLabel.font = [UIFont fontWithName:@"CCKissAndTell" size:12.0f];
        self.speechOneLabel.textColor = [UIColor blackColor];
        self.speechOneLabel.textAlignment = NSTextAlignmentCenter;
        self.speechOneLabel.text = @"IT'S OUR MISSION TO HELP YOU UNDERSTAND HEALTH AND ILLNESS.";
        [self.bubbleOneView addSubview:self.speechOneLabel];
        
        self.speechTwoLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.speechTwoLabel.numberOfLines = 0;
        self.speechTwoLabel.font = [UIFont fontWithName:@"CCKissAndTell" size:14.0f];
        self.speechTwoLabel.textColor = [UIColor blackColor];
        self.speechTwoLabel.textAlignment = NSTextAlignmentCenter;
        self.speechTwoLabel.text = @"WE ALL HAVE EXPERT KNOWLEDGE ABOUT HOW THE HUMAN BODY WORKS.";
        [self.bubbleTwoView addSubview:self.speechTwoLabel];

        self.speechThreeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.speechThreeLabel.numberOfLines = 0;
        self.speechThreeLabel.font = [UIFont fontWithName:@"CCKissAndTell" size:14.0f];
        self.speechThreeLabel.textColor = [UIColor blackColor];
        self.speechThreeLabel.textAlignment = NSTextAlignmentCenter;
        self.speechThreeLabel.text = @"AND WE'RE GOING TO HAVE A LOT OF FUN TOGETHER!";
        [self.bubbleThreeView addSubview:self.speechThreeLabel];
 */
        self.sceneView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"screen2comics"]];
        self.sceneView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:self.sceneView];

        self.transform = CGAffineTransformMakeRotation(M_PI_2);
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.sceneView.frame = self.bounds;
/* Temporary commented
    CGRect characterOneRect = CGRectZero;
    characterOneRect.size = self.characterOneView.image.size;
    characterOneRect.origin.x = kSideIndent;
    characterOneRect.origin.y = CGRectGetHeight(self.bounds) - kBottomIndent - CGRectGetHeight(characterOneRect);
    self.characterOneView.frame = characterOneRect;
    
    CGRect characterTwoRect = CGRectZero;
    characterTwoRect.size = self.characterTwoView.image.size;
    characterTwoRect.origin.x = roundf(CGRectGetWidth(self.bounds) / 2.0f) - CGRectGetWidth(characterTwoRect);
    characterTwoRect.origin.y = CGRectGetMaxY(characterOneRect) - CGRectGetHeight(characterTwoRect);
    self.characterTwoView.frame = characterTwoRect;
    
    CGRect characterThreeRect = CGRectZero;
    characterThreeRect.size = self.characterThreeView.image.size;
    characterThreeRect.origin.x = CGRectGetWidth(self.bounds) - CGRectGetWidth(characterThreeRect) - kSideIndent;
    characterThreeRect.origin.y = CGRectGetMaxY(characterOneRect) - CGRectGetHeight(characterThreeRect);
    self.characterThreeView.frame = characterThreeRect;

    CGRect bubbleOneRect = CGRectZero;
    bubbleOneRect.size = self.bubbleOneView.image.size;
    bubbleOneRect.origin.x = CGRectGetMaxX(characterOneRect) - roundf(CGRectGetWidth(bubbleOneRect) * kAllowedIntersection);
    bubbleOneRect.origin.y = CGRectGetMinY(characterOneRect) - roundf(CGRectGetHeight(bubbleOneRect) * kAllowedIntersection);
    self.bubbleOneView.frame = bubbleOneRect;
    
    CGRect speechOneRect = CGRectZero;
    speechOneRect.size = kRelativeContentSize(bubbleOneRect.size);
    speechOneRect.origin.x = roundf((CGRectGetWidth(bubbleOneRect) - CGRectGetWidth(speechOneRect)) / 2.0f);
    speechOneRect.origin.y = roundf((CGRectGetHeight(bubbleOneRect) - CGRectGetHeight(speechOneRect)) / 2.0f);
    self.speechOneLabel.frame = speechOneRect;
    
    CGRect bubbleThreeRect = CGRectZero;
    bubbleThreeRect.size = self.bubbleThreeView.image.size;
    bubbleThreeRect.origin.x = CGRectGetMinX(characterThreeRect) + roundf(CGRectGetWidth(bubbleThreeRect) * kAllowedIntersection);
    bubbleThreeRect.origin.y = CGRectGetMinY(bubbleOneRect);
    self.bubbleThreeView.frame = bubbleThreeRect;
    
    CGRect speechThreeRect = CGRectZero;
    speechThreeRect.size = kRelativeContentSize(bubbleThreeRect.size);
    speechThreeRect.origin.x = roundf((CGRectGetWidth(bubbleThreeRect) - CGRectGetWidth(speechThreeRect)) / 2.0f);
    speechThreeRect.origin.y = roundf((CGRectGetHeight(bubbleThreeRect) - CGRectGetHeight(speechThreeRect)) / 2.0f);
    self.speechThreeLabel.frame = speechThreeRect;
    
    CGRect bubbleTwoRect = CGRectZero;
    bubbleTwoRect.size = self.bubbleTwoView.image.size;
    bubbleTwoRect.origin.x = CGRectGetMaxX(characterTwoRect) - roundf(CGRectGetWidth(bubbleTwoRect) * kAllowedIntersection);
    bubbleTwoRect.origin.y = roundf((CGRectGetHeight(self.bounds) - CGRectGetHeight(bubbleTwoRect)) / 2.0f);
    self.bubbleTwoView.frame = bubbleTwoRect;
    
    CGRect speechTwoRect = CGRectZero;
    speechTwoRect.size = kRelativeContentSize(bubbleTwoRect.size);
    speechTwoRect.origin.x = roundf((CGRectGetWidth(bubbleTwoRect) - CGRectGetWidth(speechTwoRect)) / 2.0f);
    speechTwoRect.origin.y = roundf((CGRectGetHeight(bubbleTwoRect) - CGRectGetHeight(speechTwoRect)) / 2.0f);
    self.speechTwoLabel.frame = speechTwoRect;
 */
    
}

@end
