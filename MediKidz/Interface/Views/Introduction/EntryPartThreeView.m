//
//  EntryPartThreeView.m
//  MediKidz
//
//  Created by Alexey Smirnov on 6/23/16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "EntryPartThreeView.h"

#define kBottomIndent           roundf(CGRectGetHeight(self.bounds) / 4)

@interface EntryPartThreeView ()

@property (strong, nonatomic) UIImageView *sceneView;
@property (strong, nonatomic) UIButton    *imAdultButton;

@end

@implementation EntryPartThreeView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.sceneView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"screen3comics"]];
        self.sceneView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:self.sceneView];
        
        self.imAdultButton = [UIButton buttonWithType:UIButtonTypeSystem];
        [self.imAdultButton setBackgroundImage:[UIImage imageNamed:@"imAdultButtonBg"] forState:UIControlStateNormal];
        [self.imAdultButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.imAdultButton.titleLabel.font = [UIFont fontWithName:@"CCKissAndTell" size:14.0f];
        [self.imAdultButton setTitle:@"I'm A Grownup" forState:UIControlStateNormal];
        [self.imAdultButton addTarget:self
                               action:@selector(_grownupButtonTap:)
                     forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.imAdultButton];
        
        self.transform = CGAffineTransformMakeRotation(M_PI_2);
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.sceneView.frame = self.bounds;
    
    CGRect imAdultRect = CGRectZero;
    imAdultRect.size = self.imAdultButton.currentBackgroundImage.size;
    imAdultRect.origin.x = roundf((CGRectGetWidth(self.bounds) - CGRectGetWidth(imAdultRect)) / 2.0f);
    imAdultRect.origin.y = CGRectGetHeight(self.bounds) - kBottomIndent;
    self.imAdultButton.frame = imAdultRect;
}

#pragma mark - Private

- (void)_grownupButtonTap:(UIButton *)button
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(entryPartThreeViewDidTap:)])
    {
        [self.delegate entryPartThreeViewDidTap:self];
    }
}

@end
