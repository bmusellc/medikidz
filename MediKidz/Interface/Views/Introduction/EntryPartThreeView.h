//
//  EntryPartThreeView.h
//  MediKidz
//
//  Created by Alexey Smirnov on 6/23/16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EntryPartThreeView;

@protocol EntryPartThreeViewDelegate <NSObject>

- (void)entryPartThreeViewDidTap:(EntryPartThreeView *)sender;

@end

@interface EntryPartThreeView : UIView

@property (weak, nonatomic) id<EntryPartThreeViewDelegate> delegate;

@end
