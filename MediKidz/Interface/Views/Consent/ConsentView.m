//
//  ConsentView.m
//  MediKidz
//
//  Created by Alexey Smirnov on 6/23/16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "ConsentView.h"
#import "UILabel+TextMetrics.h"

#define kSideIndent                           20.0f
#define kImageHeight                          roundf(CGRectGetHeight(self.bounds) * 0.4f)
#define kVerticalIndent                       25.0f
#define kNextButtonSize                       CGSizeMake(160, 50)

@interface ConsentView ()

@property (strong, nonatomic) UIScrollView    *scrollView;
@property (strong, nonatomic) UIImageView     *imageView;
@property (strong, nonatomic) UILabel         *titleLabel;
@property (strong, nonatomic) UILabel         *summaryLabel;
@property (strong, nonatomic) UIButton        *moreButton;
@property (strong, nonatomic) UIButton        *nextButton;

@end

@implementation ConsentView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectZero];
        self.scrollView.showsHorizontalScrollIndicator = NO;
        [self addSubview:self.scrollView];
        
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.imageView.contentMode = UIViewContentModeCenter;
        [self.scrollView addSubview:self.imageView];
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.titleLabel.font = [UIFont fontWithName:@"Helvetica-Light" size:35.0f];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.numberOfLines = 0;
        self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.titleLabel.textColor = [UIColor blackColor];
        [self.scrollView addSubview:self.titleLabel];
        
        self.summaryLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.summaryLabel.font = [UIFont systemFontOfSize:16.0f];
        self.summaryLabel.textAlignment = NSTextAlignmentCenter;
        self.summaryLabel.numberOfLines = 0;
        self.summaryLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.summaryLabel.textColor = [UIColor blackColor];
        [self.scrollView addSubview:self.summaryLabel];
        
        self.moreButton = [UIButton buttonWithType:UIButtonTypeSystem];
        self.moreButton.titleLabel.numberOfLines = 0;
        self.moreButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.moreButton.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        [self.moreButton addTarget:self
                            action:@selector(_handleMoreButtonTap:)
                  forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:self.moreButton];
        
        self.nextButton = [UIButton buttonWithType:UIButtonTypeSystem];
        self.nextButton.layer.borderColor = self.nextButton.tintColor.CGColor;
        self.nextButton.layer.borderWidth = 1.0f;
        self.nextButton.layer.cornerRadius = 5.0f;
        [self.nextButton setTitle:@"Next" forState:UIControlStateNormal];
        [self.moreButton addTarget:self
                            action:@selector(_handleNextButtonTap:)
                  forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:self.nextButton];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.scrollView.frame = self.bounds;
    
    CGRect imageViewRect = CGRectZero;
    imageViewRect.origin.x = kSideIndent;
    imageViewRect.size.width = CGRectGetWidth(self.bounds) - 2 * kSideIndent;
    imageViewRect.size.height = kImageHeight;
    self.imageView.frame = imageViewRect;
    
    CGRect titleRect = CGRectZero;
    titleRect.size = [self.titleLabel sizeThatReallyFits:CGSizeMake(CGRectGetWidth(imageViewRect), CGFLOAT_MAX)];
    titleRect.origin.x = roundf((CGRectGetWidth(self.bounds) - CGRectGetWidth(titleRect)) / 2.0f);
    titleRect.origin.y = CGRectGetMaxY(imageViewRect);
    self.titleLabel.frame = titleRect;
    
    CGRect summaryRect = CGRectZero;
    summaryRect.size = [self.summaryLabel sizeThatReallyFits:CGSizeMake(CGRectGetWidth(imageViewRect), CGFLOAT_MAX)];
    summaryRect.origin.x = kSideIndent;
    summaryRect.origin.y = CGRectGetMaxY(titleRect) + kVerticalIndent;
    self.summaryLabel.frame = summaryRect;
    
    CGRect moreButtonRect = CGRectZero;
    moreButtonRect.size = [self.moreButton.titleLabel sizeThatReallyFits:CGSizeMake(CGRectGetWidth(imageViewRect), CGFLOAT_MAX)];
    moreButtonRect.origin.x = roundf((CGRectGetWidth(self.bounds) - CGRectGetWidth(moreButtonRect)) / 2.0f);
    moreButtonRect.origin.y = CGRectGetMaxY(summaryRect) + kVerticalIndent;
    self.moreButton.frame = moreButtonRect;
    
    CGRect nextButtonRect = CGRectZero;
    nextButtonRect.size = kNextButtonSize;
    nextButtonRect.origin.x = roundf((CGRectGetWidth(self.bounds) - CGRectGetWidth(nextButtonRect)) / 2.0f);
    nextButtonRect.origin.y = CGRectGetMaxY(moreButtonRect) + kVerticalIndent;
    if (CGRectGetMaxY(nextButtonRect) + kVerticalIndent < CGRectGetHeight(self.bounds))
    {
        nextButtonRect.origin.y = CGRectGetHeight(self.bounds) - CGRectGetHeight(nextButtonRect) - kVerticalIndent;
    }
    self.nextButton.frame = nextButtonRect;
    
    self.scrollView.contentSize = CGSizeMake(CGRectGetWidth(self.bounds), CGRectGetMaxY(nextButtonRect) + kVerticalIndent);
}

#pragma mark - Private

- (void)_handleMoreButtonTap:(UIButton *)moreButton
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(consentView:didTapOnMoreButton:)])
    {
        [self.delegate consentView:self didTapOnMoreButton:moreButton];
    }
}

- (void)_handleNextButtonTap:(UIButton *)nextButton
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(consentView:didTapOnNextButton:)])
    {
        [self.delegate consentView:self didTapOnNextButton:nextButton];
    }
}

@end
