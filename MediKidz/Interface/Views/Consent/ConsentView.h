//
//  ConsentView.h
//  MediKidz
//
//  Created by Alexey Smirnov on 6/23/16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ConsentView;

@protocol ConsentViewDelegate <NSObject>

- (void)consentView:(ConsentView *)consentView didTapOnNextButton:(UIButton *)nextButton;
- (void)consentView:(ConsentView *)consentView didTapOnMoreButton:(UIButton *)moreButton;

@end

@interface ConsentView : UIView

@property (weak, nonatomic) id<ConsentViewDelegate> delegate;

@end