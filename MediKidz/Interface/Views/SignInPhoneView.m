//
//  SignInPhoneView.m
//  MediKidz
//
//  Created by Mike Ponomaryov on 24.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "SignInPhoneView.h"

#define kTopMargin          20.0f
#define kBottomMargin       10.0f
#define kLabelsMargin       20.0f
#define kSignInWidth        140.0f
#define kSignInHeight       52.0f
#define kButtonsColor       RGBColor(79.0f, 90.0f, 166.0f)
#define kSignInTitle        @"Enter your phone number to sign into the app"
#define kExplainTitle       @"Your number will be encrypted before it is sent for verification"

@interface SignInPhoneView () <UITextFieldDelegate>

@property (nonatomic, strong)   UILabel     *titleLabel;
@property (nonatomic, strong)   UILabel     *explainLabel;

@end

@implementation SignInPhoneView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {        
        self.titleLabel = [UILabel new];
        self.titleLabel.font = [UIFont systemFontOfSize:22.0f];
        self.titleLabel.text = kSignInTitle;
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.numberOfLines = 0;
        [self addSubview:self.titleLabel];

        self.phoneTextField = [UITextField new];
        self.phoneTextField.delegate = self;
        self.phoneTextField.keyboardType = UIKeyboardTypeNumberPad;
        self.phoneTextField.font = [UIFont systemFontOfSize:36.0f];
        self.phoneTextField.backgroundColor = [UIColor whiteColor];
        self.phoneTextField.text = @"123 4567890";
        self.phoneTextField.textAlignment = NSTextAlignmentCenter;
        [self.phoneTextField sizeToFit];
        [self addSubview:self.phoneTextField];
        
        self.explainLabel = [UILabel new];
        self.explainLabel.font = [UIFont systemFontOfSize:12.0f];
        self.explainLabel.text = kExplainTitle;
        self.explainLabel.textAlignment = NSTextAlignmentCenter;
        self.explainLabel.numberOfLines = 0;
        [self addSubview:self.explainLabel];
        
        self.signInButton = [UIButton new];
        self.signInButton.backgroundColor = [UIColor whiteColor];
        self.signInButton.frame = CGRectMake(0, 0, kSignInWidth, kSignInHeight);
        [self.signInButton setTitle:@"Sign In" forState:UIControlStateNormal];
        [self.signInButton setTitleColor:kButtonsColor forState:UIControlStateNormal];
        self.signInButton.layer.cornerRadius = 10.0f;
        self.signInButton.layer.borderColor = kButtonsColor.CGColor;
        self.signInButton.layer.borderWidth = 1.0f;
        [self addSubview:self.signInButton];
        
        self.helpButton = [UIButton new];
        self.helpButton.titleLabel.font = [UIFont systemFontOfSize:12.0f];
        [self.helpButton setTitle:@"Help" forState:UIControlStateNormal];
        [self.helpButton setTitleColor:kButtonsColor forState:UIControlStateNormal];
        [self.helpButton sizeToFit];
        [self addSubview:self.helpButton];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGSize titleLabelSize = [self.titleLabel sizeThatFits:CGSizeMake(CGRectGetWidth(self.bounds), CGFLOAT_MAX)];
    self.titleLabel.frame = CGRectMake(0, kTopMargin, CGRectGetWidth(self.bounds), titleLabelSize.height);
    
    self.phoneTextField.center = CGPointMake(self.center.x, CGRectGetMaxY(self.titleLabel.frame) + CGRectGetHeight(self.phoneTextField.frame) / 2 + kLabelsMargin);
    
    self.signInButton.center = CGPointMake(self.center.x, CGRectGetMaxY(self.phoneTextField.frame) + CGRectGetHeight(self.signInButton.frame) / 2 + kLabelsMargin);
    
    CGSize explainLabelSize = [self.explainLabel sizeThatFits:CGSizeMake(CGRectGetWidth(self.bounds), CGFLOAT_MAX)];
    self.explainLabel.frame = CGRectMake(0, CGRectGetMaxY(self.signInButton.frame) + kLabelsMargin, CGRectGetWidth(self.bounds), explainLabelSize.height);
    
    self.helpButton.center = CGPointMake(self.center.x, CGRectGetMaxY(self.bounds) - CGRectGetHeight(self.helpButton.frame) / 2 - kLabelsMargin);
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
    BOOL allowEditing = YES;
    
    UITextPosition *pos = [self.phoneTextField positionFromPosition:self.phoneTextField.endOfDocument
                                                        inDirection:UITextLayoutDirectionLeft
                                                             offset:0];
    UITextRange *newRange = [self.phoneTextField textRangeFromPosition:pos
                                                            toPosition:pos];
    
    self.phoneTextField.selectedTextRange = newRange;
    
    if (textField.text.length > 10 && ![string isEqualToString:@""])
    {
        allowEditing = NO;
    }
    
    if (range.location != textField.text.length)
    {

//
//        allowEditing = NO;
    }
    else
    {
        if (textField.text.length == 3 && ![string isEqualToString:@""])
        {
            textField.text = [textField.text stringByAppendingString:@" "];
        }
        else if (textField.text.length > 3)
        {
            NSString *thirdDigit = [textField.text substringWithRange:NSMakeRange(3, 1)];
            if (![thirdDigit isEqualToString:@" "])
            {
                textField.text = [textField.text stringByReplacingCharactersInRange:NSMakeRange(3, 1)
                                                                         withString:[NSString stringWithFormat:@" %@", thirdDigit]];
            }
        }
    }
    
    return allowEditing;
}

@end
