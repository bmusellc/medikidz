//
//  WelcomeView.h
//  MediKidz
//
//  Created by Alexey Smirnov on 6/24/16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WelcomeView;

@protocol WelcomeViewDelegate <NSObject>

- (void)welcomeViewDidTapOnStartButton:(WelcomeView *)sender;

@end

@interface WelcomeView : UIView

@property (weak, nonatomic) id<WelcomeViewDelegate> delegate;

@end
