//
//  HomeViewController.m
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "HomeViewController.h"
#import "BasicNavigationController.h"
#import "IntroViewController.h"
#import "UIViewController+Present.h"

#define kUserSignedKey  @"UserSigned"

@implementation HomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    self.title = @"Home";
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    id isSigned = [[NSUserDefaults standardUserDefaults] objectForKey:kUserSignedKey];
    if (!isSigned)
    {
        IntroViewController *introVC = [IntroViewController new];
        [self.navigationController pushViewController:introVC animated:NO];
    }
}

@end
