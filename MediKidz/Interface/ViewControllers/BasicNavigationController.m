//
//  BasicNavigationController.m
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "BasicNavigationController.h"
#import "BasicViewController.h"
#import "ButtonedTabBarController.h"

#define kBarTintColor   RGBColor(202.0f, 28.0f, 31.0f)
@implementation BasicNavigationController

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController
{
    self = [super initWithRootViewController:rootViewController];
    if (self)
    {
        self.delegate = self;
        self.navigationBar.clipsToBounds = YES;
        
        NSDictionary *textAttribs = @{ NSForegroundColorAttributeName: kBarTintColor,
                                       NSFontAttributeName: [UIFont fontWithName:@"FjallaOne" size:18.0f] };
        
        self.navigationBar.tintColor = kBarTintColor;
        self.navigationBar.titleTextAttributes = textAttribs;
        [[UIBarButtonItem appearance] setTitleTextAttributes:textAttribs forState:UIControlStateNormal];
    }
    return self;
}

#pragma mark - UINavigationControllerDelegate methods

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    viewController.navigationController.navigationBar.userInteractionEnabled = NO;
    
    if ([viewController respondsToSelector:@selector(wantsHideTabBar)])
    {
        id topController = self.viewControllers.firstObject;
        
        if ([topController isKindOfClass:ButtonedTabBarController.class])
        {
            ButtonedTabBarController* tabCtrl = (ButtonedTabBarController *)topController;
            if ([tabCtrl respondsToSelector:@selector(setTabBarHidden:)])
            {
                BasicViewController *vc = (BasicViewController *)viewController;
                tabCtrl.tabBarHidden = [vc wantsHideTabBar];
            }
        }
    }
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    viewController.navigationController.navigationBar.userInteractionEnabled = YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


@end