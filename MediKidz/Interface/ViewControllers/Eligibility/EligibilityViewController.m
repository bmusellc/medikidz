//
//  EligibilityViewController.m
//  MediKidz
//
//  Created by Stanislav Kovalchuk on 6/24/16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "EligibilityViewController.h"
#import "EligibilityQuizCellCollectionViewCell.h"
#import "EligibilityQuizFooter.h"
#import "EligibilityFailureViewController.h"
#import <ResearchKit/ResearchKit.h>
#import "ConsentDocument.h"

#define kUserSignedKey  @"UserSigned"
#define kFooterHeight 200

@interface EligibilityViewController ()

@property (nonatomic, nonnull, strong)  UICollectionView*   collectionView;
@property (nonatomic, nonnull, strong)  NSArray*            cellTitles;
@property (nonatomic, nonnull, strong)  UIBarButtonItem*    nextButton;
@end

@interface EligibilityViewController (UICollectionViewDelegate)<UICollectionViewDelegate>@end
@interface EligibilityViewController (UICollectionViewDataSource)<UICollectionViewDataSource>@end
@interface EligibilityViewController (UICollectionViewDelegateFlowLayout)<UICollectionViewDelegateFlowLayout>@end
@interface EligibilityViewController (EligibilityQuizCellCollectionViewCellDelegate)<EligibilityQuizCellCollectionViewCellDelegate>@end
@interface EligibilityViewController (ORKTaskViewControllerDelegate)<ORKTaskViewControllerDelegate>@end

@implementation EligibilityViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Eligibility";
    
    self.nextButton = [[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(handleNextButtonTap)];
    
    self.cellTitles = @[@"Are you at least 18 years old?",
                        @"Is your child at least 6 years old and is not yet 12 years old?",
                        @"Are you the child's paren or legal guardian?"];
    
    // Do any additional setup after loading the view.
    UICollectionViewFlowLayout* layout = [UICollectionViewFlowLayout new];
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    layout.itemSize = CGSizeMake(CGRectGetWidth(self.view.bounds), 100);
    layout.minimumLineSpacing = 10;
    layout.minimumInteritemSpacing = 0;
    layout.footerReferenceSize = CGSizeMake(CGRectGetWidth(self.view.bounds), 150);
    layout.sectionInset = UIEdgeInsetsMake(0, 0, layout.minimumLineSpacing, 0);
    self.collectionView = [[UICollectionView alloc]initWithFrame:self.view.bounds collectionViewLayout:layout];
    self.collectionView.bounces = NO;
    [self.view addSubview:self.collectionView];
    self.collectionView.backgroundColor = [UIColor lightGrayColor];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    {
        NSLayoutConstraint *width =[NSLayoutConstraint
                                    constraintWithItem:self.collectionView
                                    attribute:NSLayoutAttributeWidth
                                    relatedBy:0
                                    toItem:self.view
                                    attribute:NSLayoutAttributeWidth
                                    multiplier:1.0
                                    constant:0];
        NSLayoutConstraint *height =[NSLayoutConstraint
                                     constraintWithItem:self.collectionView
                                     attribute:NSLayoutAttributeHeight
                                     relatedBy:0
                                     toItem:self.view
                                     attribute:NSLayoutAttributeHeight
                                     multiplier:1.0
                                     constant:0];
        NSLayoutConstraint *top = [NSLayoutConstraint
                                   constraintWithItem:self.collectionView
                                   attribute:NSLayoutAttributeTop
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.topLayoutGuide
                                   attribute:NSLayoutAttributeBottom
                                   multiplier:1.0f
                                   constant:0.f];
        NSLayoutConstraint *leading = [NSLayoutConstraint
                                       constraintWithItem:self.collectionView
                                       attribute:NSLayoutAttributeLeading
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:self.view
                                       attribute:NSLayoutAttributeLeading
                                       multiplier:1.0f
                                       constant:0.f];
        [self.view addConstraint:width];
        [self.view addConstraint:height];
        [self.view addConstraint:top];
        [self.view addConstraint:leading];
    }
    [self.collectionView registerNib:[UINib nibWithNibName:@"EligibilityQuizCellCollectionViewCell" bundle:nil]
          forCellWithReuseIdentifier:[EligibilityQuizCellCollectionViewCell reuseID]];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"EligibilityQuizFooter" bundle:nil]
          forSupplementaryViewOfKind:UICollectionElementKindSectionFooter
                 withReuseIdentifier:[EligibilityQuizFooter reuseID]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationItem setHidesBackButton:NO];
    self.navigationController.navigationBar.hidden = NO;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)handleNextButtonTap
{
    NSArray<EligibilityQuizCellCollectionViewCell *>* cells = [self.collectionView visibleCells];
    BOOL yesChosen = YES;
    for (EligibilityQuizCellCollectionViewCell* cell in cells)
    {
        if (![cell yesChoosen])
        {
            yesChosen = NO;
            break;
        }
    }
    
    if (!yesChosen)
    {
       // push to fial controller
        [self.navigationController pushViewController:[EligibilityFailureViewController new] animated:YES];
    }
    else
    {
        // start researchKit intro
        [self.navigationItem setHidesBackButton:YES];
        ConsentDocument *document = [ConsentDocument new];
        
        ORKVisualConsentStep *consentStep = [[ORKVisualConsentStep alloc] initWithIdentifier:@"VisualConsentStep" document:document];
        
        ORKConsentSignature *signature = [document.signatures firstObject];
        ORKConsentReviewStep *reviewStep = [[ORKConsentReviewStep alloc] initWithIdentifier:@"ConsentReviewStep"
                                                                                  signature:signature
                                                                                 inDocument:document];
        reviewStep.text = @"Review the consent form.";
        reviewStep.reasonForConsent = @"Consent to join the Medikidz Research Study.";
        
        ORKPasscodeStep *passcodeStep = [[ORKPasscodeStep alloc] initWithIdentifier:@"PasscodeStep"];
        passcodeStep.text = @"Now you will create a passcode to identify yourself to the app and protect access to information you've entered.";
        
        ORKCompletionStep *completionStep = [[ORKCompletionStep alloc] initWithIdentifier:@"CompletionStep"];
        completionStep.title = @"Welcome aboard.";
        completionStep.text = @"Thank you for joining this study.";
        
        ORKOrderedTask *orderedTask = [[ORKOrderedTask alloc] initWithIdentifier:@"Join" steps:@[consentStep, reviewStep, passcodeStep, completionStep]];
        
        ORKTaskViewController *taskVC = [[ORKTaskViewController alloc] initWithTask:orderedTask taskRunUUID:nil];
        taskVC.delegate = self;
        [self presentViewController:taskVC animated:YES completion:nil];
//        [self.navigationController pushViewController:taskVC animated:YES];
    }

}

- (void)_updateNextButton
{
    NSArray<EligibilityQuizCellCollectionViewCell *>* cells = [self.collectionView visibleCells];
    
    BOOL shouldShow = YES;
    for (EligibilityQuizCellCollectionViewCell* cell in cells)
    {
        if (![cell isChoiceMade])
        {
            shouldShow = NO;
            break;
        }
    }
    
    if (shouldShow)
    {
        self.navigationItem.rightBarButtonItem = self.nextButton;
    }
    else
    {
        self.navigationItem.rightBarButtonItem = nil;
    }
}

@end

#pragma mark - UICollectionViewDelegate
@implementation EligibilityViewController (UICollectionViewDelegate)


@end

#pragma mark - UICollectionViewDataSource
@implementation EligibilityViewController (UICollectionViewDataSource)

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionFooter)
    {
        UICollectionReusableView *footerview = [self.collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter
                                                                                       withReuseIdentifier:[EligibilityQuizFooter reuseID]
                                                                                              forIndexPath:indexPath];
        
        reusableview = footerview;
    }
    return reusableview;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 3;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    EligibilityQuizCellCollectionViewCell* cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:[EligibilityQuizCellCollectionViewCell reuseID]
                                                                                                 forIndexPath:indexPath];
    
    [cell setTitle:self.cellTitles[indexPath.row]];
    cell.delegate = self;
    return cell;
}

@end

#pragma mark - UICollectionViewDelegateFlowLayout
@implementation EligibilityViewController (UICollectionViewDelegateFlowLayout)

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat itemCount = [self.collectionView numberOfItemsInSection:0];
    CGFloat lineSpacing = [(UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout minimumLineSpacing];
    
    CGFloat itemHeight = (CGRectGetHeight(self.collectionView.frame) - self.topLayoutGuide.length - kFooterHeight - (itemCount * lineSpacing)) / itemCount;
    return CGSizeMake(CGRectGetWidth(self.collectionView.frame), itemHeight);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    CGFloat headerHeight = kFooterHeight;
    return CGSizeMake(CGRectGetWidth(self.collectionView.frame), headerHeight);
}

@end

#pragma mark - EligibilityQuizCellCollectionViewCell
@implementation EligibilityViewController (EligibilityQuizCellCollectionViewCellDelegate)

- (void)eligibilityQuizCellCollectionViewCellMadeChoice
{
    [self _updateNextButton];
}

@end

#pragma mark - ORKTaskViewControllerDelegate
@implementation EligibilityViewController (ORKTaskViewControllerDelegate)

- (void)taskViewController:(ORKTaskViewController *)taskViewController didFinishWithReason:(ORKTaskViewControllerFinishReason)reason error:(nullable NSError *)error
{
    switch (reason)
    {
        case ORKTaskViewControllerFinishReasonCompleted:
            [[NSUserDefaults standardUserDefaults] setBool:YES
                                                    forKey:kUserSignedKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self dismissViewControllerAnimated:YES completion:nil];
            [self.navigationController popToRootViewControllerAnimated:YES];
            break;
        case ORKTaskViewControllerFinishReasonDiscarded:
        case ORKTaskViewControllerFinishReasonFailed:
        case ORKTaskViewControllerFinishReasonSaved:
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        default:
            break;
    }

}

@end