//
//  EligibilityFailureViewController.m
//  MediKidz
//
//  Created by Alexey Smirnov on 6/24/16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "EligibilityFailureViewController.h"
#import "EligibilityFailureView.h"

@interface EligibilityFailureViewController ()

@property (strong, nonatomic) EligibilityFailureView    *failureView;

@end

@interface EligibilityFailureViewController (EligibilityFailureViewDelegate) <EligibilityFailureViewDelegate> @end

@implementation EligibilityFailureViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Eligibility";
    self.failureView = [[EligibilityFailureView alloc] initWithFrame:CGRectZero];
    self.failureView.delegate = self;
    [self.view addSubview:self.failureView];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    self.failureView.frame = self.view.bounds;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

- (void)dealloc
{
    self.failureView.delegate = nil;
}

@end

#pragma mark - EligibilityFailureView delegate's methods

@implementation EligibilityFailureViewController (EligibilityFailureViewDelegate)

- (void)failureViewDidTapOnOkButton:(EligibilityFailureView *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end