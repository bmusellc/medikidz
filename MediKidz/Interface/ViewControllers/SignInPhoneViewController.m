//
//  SignInPhoneViewController.m
//  MediKidz
//
//  Created by Mike Ponomaryov on 24.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "SignInPhoneViewController.h"
#import "SignInPhoneView.h"
#import "WelcomeViewController.h"

#define kBgColor            RGBColor(247.0f, 247.0f, 247.0f)

@interface SignInPhoneViewController ()

@property (nonatomic, strong)   SignInPhoneView     *signInView;
@property (nonatomic, assign)   NSUInteger          kbHeight;

@end

@implementation SignInPhoneViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = kBgColor;
    
    self.signInView = [SignInPhoneView new];
    [self.signInView.signInButton addTarget:self
                                     action:@selector(_signInTapped)
                           forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.signInView];
    
    self.kbHeight = 0;
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    CGRect signInViewRect = self.activeBounds;
    
    if (self.signInView.phoneTextField.isFirstResponder)
    {
        signInViewRect.size.height -= _kbHeight;
    }
    
    self.signInView.frame = signInViewRect;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_keyboardDidShow:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    
    [self.signInView.phoneTextField becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Private

- (void)_keyboardDidShow:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    CGSize size = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    self.kbHeight = size.height;
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
}

- (void)_keyboardDidHide:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    CGSize size = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    self.kbHeight = size.height;
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
}

- (void)_signInTapped
{
    [self.navigationController pushViewController:[WelcomeViewController new] animated:YES];
}

@end
