//
//  BasicViewController.m
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "BasicViewController.h"
#import "BasicNavigationController.h"
#import "ButtonedTabBarController.h"
#import "UIImage+Color.h"

@implementation BasicViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    return self;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateNavigationBarColor];
    self.navigationController.navigationBar.translucent = YES;
}

- (UIColor *)navigationControllerColor
{
    return [UIColor clearColor];
}

- (UIColor *)navigationBarShadowColor
{
    return [UIColor clearColor];
}

- (void)updateNavigationBarColor
{
    UIImage *navigationControllerImage = [UIImage imageWithColor:[self navigationControllerColor]];
    [self.navigationController.navigationBar setBackgroundImage:navigationControllerImage
                                                  forBarMetrics:UIBarMetricsDefault];
    UIImage *navigationShadowImage = [UIImage imageWithColor:[self navigationBarShadowColor]];
    self.navigationController.navigationBar.shadowImage = navigationShadowImage;
    self.navigationController.view.backgroundColor = [self navigationControllerColor];
    self.navigationController.navigationBar.backgroundColor = [self navigationControllerColor];
}

- (BOOL)wantsHideTabBar
{
    return NO;
}

- (BOOL)tabBar:(ButtonedTabBarView *)tabBar willSelectTabAtIndex:(NSUInteger)index
{
    return YES;
}

- (void)tabBar:(ButtonedTabBarView *)tabBar didSelectTabAtIndex:(NSUInteger)index
{

}

- (CGRect)activeBounds
{
    CGRect activeBounds = self.view.bounds;
    
    if (self.buttonedTabBar && !self.buttonedTabBar.tabBarHidden)
    {
        CGFloat tabBarHeight = self.buttonedTabBar.tabBarView.bounds.size.height;
        activeBounds.size.height -= tabBarHeight;
    }
    else if (self.navigationController && [self.navigationController isMemberOfClass:[BasicNavigationController class]])
    {
        BasicNavigationController *navCtrl = (BasicNavigationController *)self.navigationController;
        if (!navCtrl.buttonedTabBar.tabBarHidden)
        {
            CGFloat tabBarHeight = navCtrl.buttonedTabBar.tabBarView.bounds.size.height;
            activeBounds.size.height -= tabBarHeight;
        }
    }
    
    if (!self.navigationController.navigationBar.hidden)
    {
        CGFloat navBarHeight = self.navigationController.navigationBar.frame.size.height;
        activeBounds.origin.y += navBarHeight;
        activeBounds.size.height -= navBarHeight;
    }
    
    CGFloat statusBarHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
    activeBounds.origin.y += statusBarHeight;
    activeBounds.size.height -= statusBarHeight;
    
    return activeBounds;
}

@end
