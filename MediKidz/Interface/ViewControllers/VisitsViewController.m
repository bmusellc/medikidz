//
//  VisitsViewController.m
//  MediKidz
//
//  Created by Mike Ponomaryov on 23.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "VisitsViewController.h"
#import "ReminderViewController.h"

#define kContentTopMargin   80.0f

@interface VisitsViewController ()

@property (nonatomic, strong)   UIImageView     *bgImageView;
@property (nonatomic, strong)   UIScrollView    *contentScrollView;
@property (nonatomic, strong)   UIImageView     *contentImageView;
@property (nonatomic, assign)   BOOL            wasReminderShown;

@end

@implementation VisitsViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.title = @"MY VISITS";
        self.tabBarItem.image = [UIImage imageNamed:@"learnNormal"];
        self.tabBarItem.selectedImage = [UIImage imageNamed:@"learnActive"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.bgImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"homeBg"]];
    self.bgImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:self.bgImageView];
    
    self.contentScrollView = [UIScrollView new];
    [self.view addSubview:self.contentScrollView];
    
    self.contentImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"myVisitsContent"]];
    self.contentImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentScrollView addSubview:self.contentImageView];
    
    self.wasReminderShown = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    CGFloat scaleFactor = CGRectGetWidth(self.activeBounds) / CGRectGetWidth(self.contentImageView.frame);
    self.contentImageView.frame = CGRectMake(0, 0, CGRectGetWidth(self.activeBounds), CGRectGetHeight(self.contentImageView.frame) * scaleFactor);
    self.contentScrollView.contentSize = self.contentImageView.frame.size;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (!self.wasReminderShown)
    {
        self.wasReminderShown = YES;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^
        {
            if (self.isViewLoaded && self.view.window)
            {
                [self presentViewController:[ReminderViewController new] animated:YES completion:nil];
            }
        });
    }
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];

    self.bgImageView.frame = self.view.bounds;
    self.contentScrollView.frame = self.activeBounds;

    CGFloat scaleFactor = CGRectGetWidth(self.activeBounds) / CGRectGetWidth(self.contentImageView.frame);
    self.contentImageView.frame = CGRectMake(0, 0, CGRectGetWidth(self.activeBounds), CGRectGetHeight(self.contentImageView.frame) * scaleFactor);
    self.contentScrollView.contentSize = self.contentImageView.frame.size;
}

@end
