//
//  HomeViewController.h
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "ButtonedTabBarController.h"

@interface HomeViewController : ButtonedTabBarController

@end
