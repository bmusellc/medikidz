//
//  PointsViewController.m
//  MediKidz
//
//  Created by Mike Ponomaryov on 23.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "PointsViewController.h"

@interface PointsViewController ()

@property (nonatomic, strong)   UIImageView     *bgImageView;
@property (nonatomic, strong)   UIImageView     *contentImageView;

@end

@implementation PointsViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.title = @"MY POINTS";
        self.tabBarItem.image = [UIImage imageNamed:@"planNormal"];
        self.tabBarItem.selectedImage = [UIImage imageNamed:@"planActive"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.bgImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"homeBg"]];
    self.bgImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:self.bgImageView];
    
    self.contentImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pointsTable"]];
    self.contentImageView.contentMode = UIViewContentModeScaleAspectFit | UIViewContentModeTop;
    [self.view addSubview:self.contentImageView];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    self.bgImageView.frame = self.view.bounds;
    self.contentImageView.frame = self.activeBounds;
}

@end
