//
//  ComicsTabbedViewController.m
//  MediKidz
//
//  Created by Stanislav Kovalchuk on 6/23/16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "ComicsTabbedViewController.h"
#import "ComicsNavigationController.h"
#import "ButtonedTabBarController.h"
#import "UIViewController+Present.h"

@interface ComicsTabbedViewController ()

@property (nonatomic, strong) UIImageView*  bgImageView;

@end

@implementation ComicsTabbedViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.title = @"COMICS";
        self.tabBarItem.image = [UIImage imageNamed:@"playNormal"];
        self.tabBarItem.selectedImage = [UIImage imageNamed:@"playActive"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.bgImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"homeBg"]];
    self.bgImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:self.bgImageView];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    self.bgImageView.frame = self.view.bounds;
}

- (BOOL)tabBar:(ButtonedTabBarView *)tabBar willSelectTabAtIndex:(NSUInteger)index
{
    ComicsNavigationController* controller = [ComicsNavigationController comicsNavigationController];
    [controller presentOverRootViewControllerAnimated:YES completion:nil];
    
    return NO;
}

@end
