//
//  EntryPartThreeViewController.m
//  MediKidz
//
//  Created by Alexey Smirnov on 6/23/16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "EntryPartThreeViewController.h"
#import "EntryPartThreeView.h"
#import "PreSignInViewController.h"
//#import <ResearchKit/ResearchKit.h>
//#import "ConsentDocument.h"

@interface EntryPartThreeViewController ()

@property (strong, nonatomic) EntryPartThreeView    *partThreeView;

@end

@interface EntryPartThreeViewController (EntryPartThreeViewDelegate) <EntryPartThreeViewDelegate> @end

@implementation EntryPartThreeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.partThreeView = [[EntryPartThreeView alloc] initWithFrame:CGRectZero];
    self.partThreeView.delegate = self;
    [self.view addSubview:self.partThreeView];
    
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    self.partThreeView.frame = self.view.bounds;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)dealloc
{
    self.partThreeView.delegate = nil;
}

@end

#pragma mark - EntryPartThreeView delegate's methods

@implementation EntryPartThreeViewController (EntryPartThreeViewDelegate)

- (void)entryPartThreeViewDidTap:(EntryPartThreeView *)sender
{
    [self.navigationController pushViewController:[PreSignInViewController new] animated:YES];
    
/*     Insert this code in appropriate place.
    ConsentDocument *document = [ConsentDocument new];
    
    ORKVisualConsentStep *consentStep = [[ORKVisualConsentStep alloc] initWithIdentifier:@"VisualConsentStep" document:document];
    
    ORKConsentSignature *signature = [document.signatures firstObject];
    ORKConsentReviewStep *reviewStep = [[ORKConsentReviewStep alloc] initWithIdentifier:@"ConsentReviewStep"
                                                                              signature:signature
                                                                             inDocument:document];
    reviewStep.text = @"Review the consent form.";
    reviewStep.reasonForConsent = @"Consent to join the Medikidz Research Study.";
    
    ORKPasscodeStep *passcodeStep = [[ORKPasscodeStep alloc] initWithIdentifier:@"PasscodeStep"];
    passcodeStep.text = @"Now you will create a passcode to identify yourself to the app and protect access to information you've entered.";
    
    ORKCompletionStep *completionStep = [[ORKCompletionStep alloc] initWithIdentifier:@"CompletionStep"];
    completionStep.title = @"Welcome aboard.";
    completionStep.text = @"Thank you for joining this study.";
    
    ORKOrderedTask *orderedTask = [[ORKOrderedTask alloc] initWithIdentifier:@"Join" steps:@[consentStep, reviewStep, passcodeStep, completionStep]];
    
    ORKTaskViewController *taskVC = [[ORKTaskViewController alloc] initWithTask:orderedTask taskRunUUID:nil];
    [self.navigationController pushViewController:taskVC animated:YES];
*/
}

@end
