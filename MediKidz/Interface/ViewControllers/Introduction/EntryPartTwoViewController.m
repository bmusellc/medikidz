//
//  EntryPartTwoViewController.m
//  MediKidz
//
//  Created by Alexey Smirnov on 6/22/16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "EntryPartTwoViewController.h"
#import "EntryPartTwoView.h"
#import "EntryPartThreeViewController.h"

@interface EntryPartTwoViewController ()

@property (strong, nonatomic) EntryPartTwoView          *partTwoView;
@property (strong, nonatomic) UITapGestureRecognizer    *tapGestureRecognizer;

@end

@implementation EntryPartTwoViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                            action:@selector(_viewDidTap)];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view addGestureRecognizer:self.tapGestureRecognizer];
    
    self.partTwoView = [[EntryPartTwoView alloc] initWithFrame:CGRectZero];
    [self.view addSubview:self.partTwoView];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    self.partTwoView.frame = self.view.bounds;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

#pragma mark - Private

- (void)_viewDidTap
{
    EntryPartThreeViewController *threePartVC = [EntryPartThreeViewController new];
    [self.navigationController pushViewController:threePartVC animated:YES];
}

@end
