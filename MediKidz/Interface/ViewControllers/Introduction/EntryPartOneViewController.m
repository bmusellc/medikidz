//
//  EntryPartOneViewController.m
//  MediKidz
//
//  Created by Alexey Smirnov on 6/22/16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "EntryPartOneViewController.h"
#import "EntryPartOneView.h"
#import "EntryPartTwoViewController.h"

@interface EntryPartOneViewController ()

@property (strong, nonatomic) EntryPartOneView          *partOneView;
@property (strong, nonnull)   UITapGestureRecognizer    *tapGestureRecognizer;

@end

@implementation EntryPartOneViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                            action:@selector(_viewDidTap)];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view addGestureRecognizer:self.tapGestureRecognizer];
    
    self.partOneView = [[EntryPartOneView alloc] initWithFrame:CGRectZero];
    [self.view addSubview:self.partOneView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    self.partOneView.frame = self.view.bounds;
}

#pragma mark - Private

- (void)_viewDidTap
{
    EntryPartTwoViewController *twoPartVC = [EntryPartTwoViewController new];
    [self.navigationController pushViewController:twoPartVC animated:YES];
}

@end
