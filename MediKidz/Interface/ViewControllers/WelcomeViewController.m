//
//  WelcomeViewController.m
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "WelcomeViewController.h"
#import "WelcomeView.h"
#import "EligibilityViewController.h"

#define kUserSignedKey  @"UserSigned"

@interface WelcomeViewController ()

@property (strong, nonatomic) WelcomeView   *welcomeView;

@end

@interface WelcomeViewController (WelcomeViewDelegate) <WelcomeViewDelegate> @end

@implementation WelcomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.welcomeView = [[WelcomeView alloc] initWithFrame:CGRectZero];
    self.welcomeView.delegate = self;
    [self.view addSubview:self.welcomeView];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    self.welcomeView.frame = self.view.bounds;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)dealloc
{
    self.welcomeView.delegate = nil;
}

#pragma mark - Private

- (void)_signIn
{
    [[NSUserDefaults standardUserDefaults] setBool:YES
                                            forKey:kUserSignedKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end

#pragma mark - WelcomeView delegate's methods

@implementation WelcomeViewController (WelcomeViewDelegate)

- (void)welcomeViewDidTapOnStartButton:(WelcomeView *)sender
{
    [self.navigationController pushViewController:[EligibilityViewController new] animated:YES];
}

@end