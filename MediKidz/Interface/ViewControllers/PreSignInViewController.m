//
//  PreSignInViewController.m
//  MediKidz
//
//  Created by Alexey Smirnov on 6/24/16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "PreSignInViewController.h"
#import "PreSignInView.h"
#import "SignInPhoneViewController.h"

@interface PreSignInViewController ()

@property (strong, nonatomic) PreSignInView   *preSignInView;

@end

@interface PreSignInViewController (PreSignInViewDelegate) <PreSignInViewDelegate> @end

@implementation PreSignInViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.preSignInView = [[PreSignInView alloc] initWithFrame:CGRectZero];
    self.preSignInView.delegate = self;
    [self.view addSubview:self.preSignInView];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    self.preSignInView.frame = self.view.bounds;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)dealloc
{
    self.preSignInView.delegate = nil;
}

@end

#pragma mark - PreSignInView delegate's methods

@implementation PreSignInViewController (PreSignInViewDelegate)

-(void)preSignInViewDidTapOnSignButton:(PreSignInView *)sender
{
    [self.navigationController pushViewController:[SignInPhoneViewController new] animated:YES];
}

//- (void)welcomeViewDidTapOnStartButton:(PreSignInView *)sender
//{
//    
//}

@end