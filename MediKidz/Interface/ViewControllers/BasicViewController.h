//
//  BasicViewController.h
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ButtonedTabBarController, ButtonedTabBarView;

@interface BasicViewController : UIViewController

@property (nonatomic, readonly)     UIColor                     *navigationControllerColor;
@property (nonatomic, readonly)     UIColor                     *navigationBarShadowColor;

@property (nonatomic, strong)       ButtonedTabBarController    *buttonedTabBar;

- (BOOL)wantsHideTabBar;
- (BOOL)tabBar:(ButtonedTabBarView *)tabBar willSelectTabAtIndex:(NSUInteger)index;
- (void)tabBar:(ButtonedTabBarView *)tabBar didSelectTabAtIndex:(NSUInteger)index;
- (CGRect)activeBounds;

@end
