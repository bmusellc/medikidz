//
//  ComicsViewController.m
//  ORKSample
//
//  Created by Stanislav Kovalchuk on 6/17/16.
//  Copyright © 2016 Apple, Inc. All rights reserved.
//

#import "ComicsNavigationController.h"
#import "ComicsController.h"
@interface ComicsNavigationController ()

@end

@implementation ComicsNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.hidesBarsOnTap = YES;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

+ (ComicsNavigationController *)comicsNavigationController
{
    ComicsController* comicsController = [ComicsController new];
    
    ComicsNavigationController* nc = [[ComicsNavigationController alloc]initWithRootViewController:comicsController];
    
    return nc;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
