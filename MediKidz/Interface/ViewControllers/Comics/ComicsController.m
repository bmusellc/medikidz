//
//  ViewController.m
//  PDFPreview
//
//  Created by Stanislav Kovalchuk on 6/14/16.
//  Copyright © 2016 Stanislav Kovalchuk. All rights reserved.
//

#import "ComicsController.h"
#import "PDFPage.h"
#import "PDFPageCell.h"
#import "PDFDocumentInfo.h"
#import "LevelUpPopover.h"

#define kPDFURL        [[NSBundle mainBundle]URLForResource:@"Asthma_part-1_(15-04-15)" withExtension:@"pdf"]
#define kOverlayColor [UIColor whiteColor]
#define kLevelUpPopoverWasShownKey @"kLevelUpPopoverWasShownKey"

@interface ComicsController ()

@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) NSArray<PDFPageCell *>* pages;

@property (strong, nonatomic) PDFDocumentInfo*          pdfInfo;
@property (nonatomic, assign) CGRect                    selectedRect;
@property (nonatomic, strong) PDFPageCell*              zoomingView;
@property (nonatomic, strong) UIView*                   containerView;
@property (nonatomic, strong) NSLayoutConstraint*       containerWidth;
@property (nonatomic, strong) NSLayoutConstraint*       containerHeight;

@property(nonnull, strong) NSLayoutConstraint* topOverlayHeight;
@property(nonnull, strong) NSLayoutConstraint* bottomOverlayHeight;
@property(nonnull, strong) NSLayoutConstraint* leftOverlaywidth;
@property(nonnull, strong) NSLayoutConstraint* rightOverlaywidth;

@property(nonatomic, strong)   LevelUpPopover*  levelUpPopover;

@property (nonatomic, assign) NSInteger numberOfPanelsScrolled;


@end

@interface ComicsController (PDFPageCellDelegate)<PDFPageCellDelegate>@end

@interface ComicsController (UIScrollViewDelegate)<UIScrollViewDelegate>@end

@interface ComicsController (LevelUpPopoverDelegate)<LevelUpPopoverDelegate>@end
@implementation ComicsController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.pdfInfo = [[PDFDocumentInfo alloc]initWithURL:kPDFURL];

    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.scrollView = [UIScrollView new];
    [self.view addSubview:self.scrollView];
    self.scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    {
        NSLayoutConstraint *width =[NSLayoutConstraint
                                    constraintWithItem:self.scrollView
                                    attribute:NSLayoutAttributeWidth
                                    relatedBy:0
                                    toItem:self.view
                                    attribute:NSLayoutAttributeWidth
                                    multiplier:1.0
                                    constant:0];
        NSLayoutConstraint *height =[NSLayoutConstraint
                                     constraintWithItem:self.scrollView
                                     attribute:NSLayoutAttributeHeight
                                     relatedBy:0
                                     toItem:self.view
                                     attribute:NSLayoutAttributeHeight
                                     multiplier:1.0
                                     constant:0];
        NSLayoutConstraint *top = [NSLayoutConstraint
                                   constraintWithItem:self.scrollView
                                   attribute:NSLayoutAttributeBottom
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.view
                                   attribute:NSLayoutAttributeBottom
                                   multiplier:1.0f
                                   constant:0.f];
        NSLayoutConstraint *leading = [NSLayoutConstraint
                                       constraintWithItem:self.scrollView
                                       attribute:NSLayoutAttributeLeading
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:self.view
                                       attribute:NSLayoutAttributeLeading
                                       multiplier:1.0f
                                       constant:0.f];
        [self.view addConstraint:width];
        [self.view addConstraint:height];
        [self.view addConstraint:top];
        [self.view addConstraint:leading];
    }
    
    self.containerView = [UIView new];
    [self.scrollView addSubview:self.containerView];
    self.containerView.translatesAutoresizingMaskIntoConstraints = NO;
    {
        NSLayoutConstraint *top = [NSLayoutConstraint
                                   constraintWithItem:self.containerView
                                   attribute:NSLayoutAttributeTop
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.scrollView
                                   attribute:NSLayoutAttributeTop
                                   multiplier:1.0 constant:0];
        NSLayoutConstraint *leading = [NSLayoutConstraint
                                        constraintWithItem:self.containerView
                                        attribute:NSLayoutAttributeLeading
                                        relatedBy:NSLayoutRelationEqual
                                        toItem:self.scrollView
                                        attribute:NSLayoutAttributeLeading
                                        multiplier:1.0 constant:0];
        self.containerHeight = [NSLayoutConstraint
                                constraintWithItem:self.containerView
                                attribute:NSLayoutAttributeHeight
                                relatedBy:NSLayoutRelationEqual
                                toItem:nil
                                attribute:NSLayoutAttributeNotAnAttribute
                                multiplier:1.0 constant:20];
        
        self.containerWidth = [NSLayoutConstraint
                                        constraintWithItem:self.containerView
                                        attribute:NSLayoutAttributeWidth
                                        relatedBy:NSLayoutRelationEqual
                                        toItem:nil
                                        attribute:NSLayoutAttributeNotAnAttribute
                                        multiplier:1.0 constant:20];
        [self.containerView addConstraint:self.containerHeight];
        [self.containerView addConstraint:self.containerWidth];
        [self.scrollView addConstraint:top];
        [self.scrollView addConstraint:leading];
        
    }
    
    UIView* pagesContainer = self.containerView;
//    UIView* pagesContainer = self.scrollView;
    NSMutableArray<PDFPageCell *>* pages = [NSMutableArray array];
    
    for (int i = 0; i < self.pdfInfo.numberOfPages; i++)
    {
        PDFPageCell* cell = [[[NSBundle mainBundle]loadNibNamed:@"PDFPageCell"
                                      owner:self
                                    options:nil] firstObject];
        [pagesContainer addSubview:cell];
        cell.translatesAutoresizingMaskIntoConstraints = NO;
        
        NSLayoutConstraint* height = [NSLayoutConstraint
                                      constraintWithItem:cell
                                      attribute:NSLayoutAttributeHeight
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:nil
                                      attribute:NSLayoutAttributeNotAnAttribute
                                      multiplier:1.0
                                      constant:self.pdfInfo.pageSize.height];
        NSLayoutConstraint* width = [NSLayoutConstraint
                                      constraintWithItem:cell
                                      attribute:NSLayoutAttributeWidth
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:nil
                                      attribute:NSLayoutAttributeNotAnAttribute
                                      multiplier:1.0
                                      constant:self.pdfInfo.pageSize.width];
        NSLayoutConstraint *centerY = [NSLayoutConstraint
                                   constraintWithItem:cell
                                   attribute:NSLayoutAttributeCenterY
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:pagesContainer
                                   attribute:NSLayoutAttributeCenterY
                                   multiplier:1.0 constant:0];
        
        if (i == 0)
        {
            NSLayoutConstraint *leading = [NSLayoutConstraint
                                           constraintWithItem:cell
                                           attribute:NSLayoutAttributeLeading
                                           relatedBy:NSLayoutRelationEqual
                                           toItem:pagesContainer
                                           attribute:NSLayoutAttributeLeading
                                           multiplier:1.0f
                                           constant:0.f];
            [self.scrollView addConstraint:leading];
        }
        else
        {
            [ self.scrollView addConstraint:[NSLayoutConstraint constraintWithItem: cell
                                                                    attribute: NSLayoutAttributeLeft
                                                                    relatedBy: NSLayoutRelationEqual
                                                                       toItem: pages[i - 1]
                                                                    attribute: NSLayoutAttributeRight
                                                                   multiplier: 1
                                                                     constant: 0 ] ];
        }
        [pagesContainer addConstraint:centerY];
        [cell addConstraint:width];
        [cell addConstraint:height];
//        cell.layer.borderColor = [UIColor greenColor].CGColor;
//        cell.layer.borderWidth = 2;
        
        cell.pdfPage.pdf = self.pdfInfo.pdfDocument;
        cell.pdfPage.pageNumber = i + 1;
        [cell.pdfPage reload];
        cell.frames = self.pdfInfo.frames[i];
        cell.delegate = self;
        [pages addObject:cell];
    }
    self.pages = [pages copy];
    
    self.scrollView.contentInset = UIEdgeInsetsMake(256, 256, 256, 256);
    self.scrollView.minimumZoomScale = 0.5f;
    self.scrollView.maximumZoomScale = 10;
    self.scrollView.delegate = self;
    
    [self _addOverlays];
    
    UISwipeGestureRecognizer* swipeRightRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(_handleSwipeRightRecognizer:)];
    swipeRightRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.scrollView addGestureRecognizer:swipeRightRecognizer];
    
    UISwipeGestureRecognizer* swipeLeftRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(_handleSwipeLeftRecognizer:)];
    swipeLeftRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [self.scrollView addGestureRecognizer:swipeLeftRecognizer];
    
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.scrollView.alpha = 0;
    
    UIBarButtonItem* closeButt = [[UIBarButtonItem alloc]initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(handleCloseButtonTap)];
    self.navigationItem.leftBarButtonItem = closeButt;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    {//to prevent manual zooming and paning
//        self.collectionView.pinchGestureRecognizer.enabled = NO;
        self.scrollView.pinchGestureRecognizer.enabled = NO;
        self.scrollView.panGestureRecognizer.enabled = NO;
    }
    CGSize contentSize = CGSizeMake(CGRectGetMaxX([self.pages lastObject].frame), CGRectGetHeight([self.pages lastObject].frame));
    self.scrollView.contentSize = contentSize;
    self.containerWidth.constant = self.scrollView.contentSize.width;
    self.containerHeight.constant = self.scrollView.contentSize.height;

    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    
    [self _handleSwipeRightRecognizer:nil];//to select first frame
    [UIView animateWithDuration:0.3 delay:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^
    {
        self.scrollView.alpha = 1;
    } completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private
- (void)handleCloseButtonTap
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)_handleSwipeLeftRecognizer:(UISwipeGestureRecognizer *)recognizer
{
    if (!self.zoomingView)
    {
        return;
    }
    
    self.numberOfPanelsScrolled -= 1;
    
    if (![self.zoomingView highlightPreviousFrame])
    {
        if ([self.zoomingView isEqual:[self.pages firstObject]])
        {
            NSLog(@"comics started");
        }
        else
        {
            NSUInteger index = [self.pages indexOfObject:self.zoomingView];
            self.zoomingView = self.pages[index - 1];
            [self _handleSwipeLeftRecognizer:recognizer];
        }
    }
}

- (void)_handleSwipeRightRecognizer:(UISwipeGestureRecognizer *)recognizer
{
    if (!self.zoomingView)
    {
        self.zoomingView = [self.pages firstObject];
    }
    
    self.numberOfPanelsScrolled += 1;
    
    if (![self.zoomingView highlightNextFrame])
    {
        if ([self.zoomingView isEqual:[self.pages lastObject]])
        {
            NSLog(@"comics ended");
        }
        else
        {
            NSUInteger index = [self.pages indexOfObject:self.zoomingView];
            self.zoomingView = self.pages[index + 1];
            [self _handleSwipeRightRecognizer:recognizer];
        }
    }
}

- (void)_addOverlays
{
    CGFloat defaultOverlaySize = 0.0;
    {
        UIView* topOverlay = [UIView new];
        topOverlay.backgroundColor = kOverlayColor;
        [self.containerView addSubview:topOverlay];
        topOverlay.translatesAutoresizingMaskIntoConstraints = NO;
        self.topOverlayHeight = [NSLayoutConstraint
                                 constraintWithItem:topOverlay
                                 attribute:NSLayoutAttributeHeight
                                 relatedBy:NSLayoutRelationEqual
                                 toItem:nil
                                 attribute:NSLayoutAttributeNotAnAttribute
                                 multiplier:1.0
                                 constant:defaultOverlaySize];
        NSLayoutConstraint *top = [NSLayoutConstraint
                                   constraintWithItem:topOverlay
                                   attribute:NSLayoutAttributeTop
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.containerView
                                   attribute:NSLayoutAttributeTop
                                   multiplier:1.0 constant:0];
        NSLayoutConstraint *leading = [NSLayoutConstraint
                                       constraintWithItem:topOverlay
                                       attribute:NSLayoutAttributeLeading
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:self.containerView
                                       attribute:NSLayoutAttributeLeading
                                       multiplier:1.0f
                                       constant:0.f];
        NSLayoutConstraint *trailing = [NSLayoutConstraint
                                        constraintWithItem:topOverlay
                                        attribute:NSLayoutAttributeTrailing
                                        relatedBy:NSLayoutRelationEqual
                                        toItem:self.containerView
                                        attribute:NSLayoutAttributeTrailing
                                        multiplier:1.0f
                                        constant:0.f];
        
        [topOverlay addConstraint:self.topOverlayHeight];
        [self.containerView addConstraint:trailing];
        [self.containerView addConstraint:top];
        [self.containerView addConstraint:leading];
    }
    {
        UIView* bottomOverlay = [UIView new];
        bottomOverlay.backgroundColor = kOverlayColor;
        [self.containerView addSubview:bottomOverlay];
        bottomOverlay.translatesAutoresizingMaskIntoConstraints = NO;
        self.bottomOverlayHeight = [NSLayoutConstraint
                                    constraintWithItem:bottomOverlay
                                    attribute:NSLayoutAttributeHeight
                                    relatedBy:NSLayoutRelationEqual
                                    toItem:nil
                                    attribute:NSLayoutAttributeNotAnAttribute
                                    multiplier:1.0
                                    constant:defaultOverlaySize];
        NSLayoutConstraint *bottom = [NSLayoutConstraint
                                      constraintWithItem:bottomOverlay
                                      attribute:NSLayoutAttributeBottom
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:self.containerView
                                      attribute:NSLayoutAttributeBottom
                                      multiplier:1.0 constant:0];
        NSLayoutConstraint *leading = [NSLayoutConstraint
                                       constraintWithItem:bottomOverlay
                                       attribute:NSLayoutAttributeLeading
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:self.containerView
                                       attribute:NSLayoutAttributeLeading
                                       multiplier:1.0f
                                       constant:0.f];
        NSLayoutConstraint *trailing = [NSLayoutConstraint
                                        constraintWithItem:bottomOverlay
                                        attribute:NSLayoutAttributeTrailing
                                        relatedBy:NSLayoutRelationEqual
                                        toItem:self.containerView
                                        attribute:NSLayoutAttributeTrailing
                                        multiplier:1.0f
                                        constant:0.f];
        
        [bottomOverlay addConstraint:self.bottomOverlayHeight];
        [self.containerView addConstraint:trailing];
        [self.containerView addConstraint:bottom];
        [self.containerView addConstraint:leading];
    }
    {
        UIView* leftOverlay = [UIView new];
        leftOverlay.backgroundColor = kOverlayColor;
        [self.containerView addSubview:leftOverlay];
        leftOverlay.translatesAutoresizingMaskIntoConstraints = NO;
        self.leftOverlaywidth = [NSLayoutConstraint
                                 constraintWithItem:leftOverlay
                                 attribute:NSLayoutAttributeWidth
                                 relatedBy:NSLayoutRelationEqual
                                 toItem:nil
                                 attribute:NSLayoutAttributeNotAnAttribute
                                 multiplier:1.0
                                 constant:defaultOverlaySize];
        NSLayoutConstraint *bottom = [NSLayoutConstraint
                                      constraintWithItem:leftOverlay
                                      attribute:NSLayoutAttributeBottom
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:self.containerView
                                      attribute:NSLayoutAttributeBottom
                                      multiplier:1.0 constant:0];
        NSLayoutConstraint *top = [NSLayoutConstraint
                                   constraintWithItem:leftOverlay
                                   attribute:NSLayoutAttributeTop
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.containerView
                                   attribute:NSLayoutAttributeTop
                                   multiplier:1.0 constant:0];
        
        NSLayoutConstraint *leading = [NSLayoutConstraint
                                       constraintWithItem:leftOverlay
                                       attribute:NSLayoutAttributeLeading
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:self.containerView
                                       attribute:NSLayoutAttributeLeading
                                       multiplier:1.0f
                                       constant:0.f];
        
        [leftOverlay addConstraint:self.leftOverlaywidth];
        [self.containerView addConstraint:top];
        [self.containerView addConstraint:bottom];
        [self.containerView addConstraint:leading];
    }
    {
        UIView* rightOverlay = [UIView new];
        rightOverlay.backgroundColor = kOverlayColor;
        [self.containerView addSubview:rightOverlay];
        rightOverlay.translatesAutoresizingMaskIntoConstraints = NO;
        self.rightOverlaywidth = [NSLayoutConstraint
                                  constraintWithItem:rightOverlay
                                  attribute:NSLayoutAttributeWidth
                                  relatedBy:NSLayoutRelationEqual
                                  toItem:nil
                                  attribute:NSLayoutAttributeNotAnAttribute
                                  multiplier:1.0
                                  constant:defaultOverlaySize];
        NSLayoutConstraint *bottom = [NSLayoutConstraint
                                      constraintWithItem:rightOverlay
                                      attribute:NSLayoutAttributeBottom
                                      relatedBy:NSLayoutRelationEqual
                                      toItem:self.containerView
                                      attribute:NSLayoutAttributeBottom
                                      multiplier:1.0 constant:0];
        NSLayoutConstraint *top = [NSLayoutConstraint
                                   constraintWithItem:rightOverlay
                                   attribute:NSLayoutAttributeTop
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.containerView
                                   attribute:NSLayoutAttributeTop
                                   multiplier:1.0 constant:0];
        
        NSLayoutConstraint *trailing = [NSLayoutConstraint
                                        constraintWithItem:rightOverlay
                                        attribute:NSLayoutAttributeTrailing
                                        relatedBy:NSLayoutRelationEqual
                                        toItem:self.containerView
                                        attribute:NSLayoutAttributeTrailing
                                        multiplier:1.0f
                                        constant:0.f];
        
        [rightOverlay addConstraint:self.rightOverlaywidth];
        [self.containerView addConstraint:top];
        [self.containerView addConstraint:bottom];
        [self.containerView addConstraint:trailing];
    }
    
}

- (void)_highlightFrameWithOverlays:(CGRect)frame animated:(BOOL)animated
{
    self.topOverlayHeight.constant = CGRectGetMinY(frame);
    self.bottomOverlayHeight.constant = CGRectGetHeight(self.containerView.bounds) - CGRectGetMaxY(frame);
    self.leftOverlaywidth.constant = CGRectGetMinX(frame);
    self.rightOverlaywidth.constant = CGRectGetWidth(self.containerView.bounds) - CGRectGetMaxX(frame);
    
    [self.view setNeedsLayout];
    if (animated)
    {
        [UIView animateWithDuration:0.3
                         animations:^
         {
             [self.view layoutIfNeeded];
         }];
    }
}

- (void)_showLevelUpPopup
{
    if (self.levelUpPopover)
    {
        return;
    }

    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults boolForKey:kLevelUpPopoverWasShownKey])
    {
        return;
    }
    
    [defaults setBool:YES forKey:kLevelUpPopoverWasShownKey];
    [defaults synchronize];
    
    self.levelUpPopover = [[[NSBundle mainBundle]loadNibNamed:@"LevelUpPopover"
                                                        owner:self
                                                      options:nil] firstObject];
    
    [self.view addSubview:self.levelUpPopover];
    self.levelUpPopover.translatesAutoresizingMaskIntoConstraints = NO;
    {
        NSLayoutConstraint *width =[NSLayoutConstraint
                                    constraintWithItem:self.levelUpPopover
                                    attribute:NSLayoutAttributeWidth
                                    relatedBy:0
                                    toItem:self.view
                                    attribute:NSLayoutAttributeWidth
                                    multiplier:1.0
                                    constant:0];
        NSLayoutConstraint *height =[NSLayoutConstraint
                                     constraintWithItem:self.levelUpPopover
                                     attribute:NSLayoutAttributeHeight
                                     relatedBy:0
                                     toItem:self.view
                                     attribute:NSLayoutAttributeHeight
                                     multiplier:1.0
                                     constant:0];
        NSLayoutConstraint *top = [NSLayoutConstraint
                                   constraintWithItem:self.levelUpPopover
                                   attribute:NSLayoutAttributeTop
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.view
                                   attribute:NSLayoutAttributeTop
                                   multiplier:1.0f
                                   constant:0.f];
        NSLayoutConstraint *leading = [NSLayoutConstraint
                                       constraintWithItem:self.levelUpPopover
                                       attribute:NSLayoutAttributeLeading
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:self.view
                                       attribute:NSLayoutAttributeLeading
                                       multiplier:1.0f
                                       constant:0.f];
        
        [self.view addConstraint:width];
        [self.view addConstraint:height];
        [self.view addConstraint:top];
        [self.view addConstraint:leading];
    }
    
    self.levelUpPopover.alpha = 0;
    self.levelUpPopover.delegate = self;
    [UIView animateWithDuration:0.3 animations:^
    {
        self.levelUpPopover.alpha = 1;
    }];
    
}

@end

#pragma mark - PDFPageCellDelegate
@implementation ComicsController (LevelUpPopoverDelegate)

- (void)levelUpPopoverShouldClose:(LevelUpPopover *)sender
{
    [UIView animateWithDuration:0.3 animations:^
    {
        self.levelUpPopover.alpha = 0.0f;
    } completion:^(BOOL finished)
    {
        self.levelUpPopover.delegate = nil;
        self.levelUpPopover = nil;
    }];
}

@end

#pragma mark - PDFPageCellDelegate
@implementation ComicsController (PDFPageCellDelegate)

- (void)pdfCell:(PDFPageCell *)sender asksToHighlightFrame:(CGRect)frame
{
//    if (self.scrollView.zoomScale > 1)
//    {
//        [self.scrollView setZoomScale:1 animated:YES];
//        self.zoomingView = nil;
//        [self.view setNeedsLayout];
//        return;
//    }

//    [self.scrollView setZoomScale:1 animated:NO];
    CGRect transformedRect = [self.scrollView convertRect:frame fromView:sender.pdfPage];
    CGRect scrollRect = transformedRect;
    scrollRect.origin.y = 0;
    scrollRect.size.height = CGRectGetHeight(self.scrollView.bounds);
    scrollRect.size.width = CGRectGetWidth(self.scrollView.bounds);
    
    transformedRect.origin.x /= self.scrollView.zoomScale;
    transformedRect.origin.y /= self.scrollView.zoomScale;
    transformedRect.size.width /= self.scrollView.zoomScale;
    transformedRect.size.height /= self.scrollView.zoomScale;
    [UIView animateWithDuration:0.7 animations:^
    {
        [self.scrollView zoomToRect:transformedRect animated:NO];
        [self _highlightFrameWithOverlays:transformedRect animated:NO];
        [self.scrollView layoutIfNeeded];
    } completion:^(BOOL finished)
    {
        if (self.numberOfPanelsScrolled == 12)
        {
            [self _showLevelUpPopup];
        }
    }];
}

@end

@implementation ComicsController (UIScrollViewDelegate)

- (nullable UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.containerView ?: nil;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
//    NSLog(@"zooming. contentSize %@ contentOffset %@", NSStringFromCGSize(self.scrollView.contentSize),  NSStringFromCGPoint(self.scrollView.contentOffset));
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(nullable UIView *)view atScale:(CGFloat)scale
{
}
@end
