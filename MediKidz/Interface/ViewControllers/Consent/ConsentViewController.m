//
//  ConsentViewController.m
//  MediKidz
//
//  Created by Alexey Smirnov on 6/23/16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "ConsentViewController.h"

@interface ConsentViewController ()

@property (strong, nonatomic)   ConsentView     *consentView;

@end


@implementation ConsentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.consentView = [[ConsentView alloc] initWithFrame:CGRectZero];
    self.consentView.delegate = self;
    [self.view addSubview:self.consentView];
}

- (void)dealloc
{
    self.consentView.delegate = nil;
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    self.consentView.frame = self.view.bounds;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

@end

#pragma mark - ConsentView delegate's methods

@implementation ConsentViewController (ConsentViewDelegate)

- (void)consentView:(ConsentView *)consentView didTapOnNextButton:(UIButton *)nextButton
{
    ENSURE_METHOD_IS_OVERRIDEN;
}

- (void)consentView:(ConsentView *)consentView didTapOnMoreButton:(UIButton *)moreButton
{
    ENSURE_METHOD_IS_OVERRIDEN;
}

@end
