//
//  ConsentViewController.h
//  MediKidz
//
//  Created by Alexey Smirnov on 6/23/16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "BasicViewController.h"
#import "ConsentView.h"

@interface ConsentViewController : BasicViewController

@property (readonly, nonatomic)     ConsentView     *consentView;

@end

@interface ConsentViewController (ConsentViewDelegate) <ConsentViewDelegate> @end

