//
//  BasicNavigationController.h
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ButtonedTabBarController;

@interface BasicNavigationController : UINavigationController <UINavigationControllerDelegate>

@property (nonatomic, strong)       ButtonedTabBarController          *buttonedTabBar;

@end
