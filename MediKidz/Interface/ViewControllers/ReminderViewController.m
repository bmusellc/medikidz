//
//  ReminderViewController.m
//  MediKidz
//
//  Created by Mike Ponomaryov on 23.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "ReminderViewController.h"

@interface ReminderViewController ()

@property (nonatomic, strong)   UIImageView     *bgImageView;

@end

@implementation ReminderViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(_dismiss)];
    
    self.bgImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"reminder"]];
    self.bgImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.bgImageView.userInteractionEnabled = YES;
    [self.bgImageView addGestureRecognizer:tapRecognizer];
    [self.view addSubview:self.bgImageView];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    self.bgImageView.frame = self.view.bounds;
}

#pragma mark - Private

- (void)_dismiss
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
