//
//  IntroViewController.m
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "IntroViewController.h"
#import "IntroView.h"
#import "WelcomeViewController.h"
#import "EntryPartOneViewController.h"
#import "SignInPhoneViewController.h"
@interface IntroViewController ()

@property (nonatomic, strong)   IntroView   *introView;

@end


@implementation IntroViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.hidesBackButton = YES;
    
    self.introView = [IntroView new];
    [self.introView.adultButton addTarget:self
                                   action:@selector(_adultButtonTapped)
                         forControlEvents:UIControlEventTouchUpInside];
    [self.introView.kidButton addTarget:self
                                 action:@selector(_kidButtonTapped)
                       forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.introView];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    self.introView.frame = self.view.bounds;
}

#pragma mark - Private

- (void)_adultButtonTapped
{
    [self.navigationController pushViewController:[SignInPhoneViewController new] animated:YES];
}

- (void)_kidButtonTapped
{
    [self.navigationController pushViewController:[EntryPartOneViewController new] animated:YES];
}

@end
