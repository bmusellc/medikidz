//
//  ButtonedTab.h
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ButtonedTab : UIButton

- (instancetype)initWithImage:(UIImage*)image highlightedImage:(UIImage *)highlightedImage title:(NSString*)title;

@end
