//
//  ButtonedTabBarView.m
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "ButtonedTabBarView.h"

@interface ButtonedTabBarView ()

@property (nonatomic, strong)               UIImageView*    bgView;
@property (nonatomic, assign, readwrite)    CGRect          activeBounds;

@end

@implementation ButtonedTabBarView

- (instancetype)initWithBackgroundImage:(UIImage*)image
{
    self = [super initWithFrame:CGRectZero];
    if (self)
    {
        self.backgroundColor = [UIColor blackColor];
        self.bgView = [[UIImageView alloc] initWithImage:image];
        self.bgView.contentMode = UIViewContentModeScaleToFill;
        self.bgView.userInteractionEnabled = YES;
        [self addSubview:self.bgView];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.bgView.frame = self.bounds;
    
    @synchronized(self.tabs)
    {
        if (self.tabs.count == 0)
        {
            return;
        }
        
        CGRect (^roundRect)(CGRect) = ^(CGRect rect)
        {
            rect.origin.x = roundf(rect.origin.x);
            rect.origin.y = roundf(rect.origin.y);
            rect.size.width = roundf(rect.size.width);
            rect.size.height = roundf(rect.size.height);
            return rect;
        };
        
        NSUInteger tabsCount = self.tabs.count;
        CGFloat desiredTabWidth = CGRectGetWidth(self.bounds) / tabsCount;
        CGFloat desiredTabHeight = CGRectGetHeight(self.bounds);
        __block CGFloat maxTabHeight = self.bounds.size.height;
        
        [self.tabs enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
        {
            ButtonedTab* tab = (ButtonedTab*)obj;
            
            CGRect tabRect = CGRectMake(self.bounds.origin.x + desiredTabWidth * idx, self.bounds.origin.y, desiredTabWidth-1, desiredTabHeight);
            CGFloat actualTabHeight = tab.bounds.size.height + tab.imageEdgeInsets.bottom;
            
            if (actualTabHeight > desiredTabHeight)
            {
                tabRect.origin.y = CGRectGetMaxY(self.bounds) - actualTabHeight;
                tabRect.size.height = actualTabHeight;
            }
            else
            {
                tabRect.origin.y = (CGRectGetMaxY(self.bounds) - desiredTabHeight) / 2;
                tabRect.size.height = desiredTabHeight;
            }

            tab.frame = roundRect(tabRect);
            [tab setNeedsLayout];
            
            maxTabHeight = MAX(maxTabHeight, tabRect.size.height);
        }];
        
        CGRect activeBounds = self.bounds;
        activeBounds.size.height = maxTabHeight;
        self.activeBounds = activeBounds;
    }
}

- (void)setTabs:(NSArray *)tabs
{
    if (self.tabs != tabs)
    {
        for (ButtonedTab* oldTab in tabs)
        {
            [self _removeTab:oldTab];
        }
        
        _tabs = tabs;
        
        for (ButtonedTab* tab in self.tabs)
        {
            [self _addTab:tab];
        }
        
        [self setNeedsLayout];
    }
}

- (void)setSelectedTab:(ButtonedTab *)selectedTab
{
    if (self.selectedTab != selectedTab)
    {
        _selectedTab.selected = NO;
        _selectedTab = selectedTab;
        _selectedTab.selected = YES;
    }
}

- (NSUInteger)selectedTabIndex
{
    return [self.tabs indexOfObject:self.selectedTab];
}

- (void)setSelectedTabIndex:(NSUInteger)selectedTabIndex
{
    [self _selectTab:self.tabs[selectedTabIndex]];
}

#pragma mark - Private

- (void)_selectTab:(ButtonedTab *)senderTab
{
	BOOL isTabSelectionAllowed = YES;
	NSUInteger tabIndex = [self.tabs indexOfObject:senderTab];
	
	if (self.delegate && [self.delegate respondsToSelector:@selector(tabBar:willSelectTabAtIndex:)])
	{
		isTabSelectionAllowed = [self.delegate tabBar:self willSelectTabAtIndex:tabIndex];
	}
	
	if (isTabSelectionAllowed)
	{
		self.selectedTab = senderTab;
		if (self.delegate && [self.delegate respondsToSelector:@selector(tabBar:didSelectTabAtIndex:)])
		{
			[self.delegate tabBar:self didSelectTabAtIndex:tabIndex];
		}
	}
}

- (void)_addTab:(ButtonedTab*)tab
{
    [tab addTarget:self action:@selector(_selectTab:) forControlEvents:UIControlEventTouchUpInside];
    tab.enabled = YES;
    tab.userInteractionEnabled = YES;
    [self addSubview:tab];
}

- (void)_removeTab:(ButtonedTab*)tab
{
    [tab removeTarget:self action:@selector(_selectTab:) forControlEvents:UIControlEventTouchUpInside];
    tab.enabled = NO;
    tab.userInteractionEnabled = NO;
    [tab removeFromSuperview];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    for (UIView *subview in self.subviews)
    {
        if (subview == self.bgView)
        {
            continue;
        }
        
        if (CGRectContainsPoint(subview.frame, point))
        {
            if (!self.hidden && self.alpha == 1.0f)
            {
                return subview;
            }
        }
    }
    
    // use this to pass the 'touch' onward in case no subviews trigger the touch
    return [super hitTest:point withEvent:event];
}

/*
- (UIView*)hitTest:(CGPoint)point withEvent:(UIEvent*)event
{
    UIView* hitView = [super hitTest:point withEvent:event];
    if (hitView != nil)
    {
        [self.superview bringSubviewToFront:self];
    }
    return hitView;
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent*)event
{
    CGRect rect = self.bounds;
    BOOL isInside = CGRectContainsPoint(rect, point);
    if(!isInside)
    {
        for (UIView *view in self.subviews)
        {
            isInside = CGRectContainsPoint(view.frame, point);
            if(isInside)
                break;
        }
    }
    return isInside;
}
*/

@end
