//
//  ButtonedTabBarView.h
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ButtonedTab.h"

@protocol ButtonedTabBarViewDelegate;

@interface ButtonedTabBarView : UIView

@property (nonatomic, strong)           NSArray*                        tabs;
@property (nonatomic, strong)           ButtonedTab*                    selectedTab;
@property (nonatomic, assign)           NSUInteger                      selectedTabIndex;

@property (nonatomic, assign, readonly) CGRect                          activeBounds;

@property (nonatomic, weak)             id<ButtonedTabBarViewDelegate>  delegate;

- (instancetype)initWithBackgroundImage:(UIImage*)image;

@end

@protocol ButtonedTabBarViewDelegate <NSObject>

@optional

- (BOOL)tabBar:(ButtonedTabBarView *)tabBar willSelectTabAtIndex:(NSUInteger)index;

@required

- (void)tabBar:(ButtonedTabBarView *)tabBar didSelectTabAtIndex:(NSUInteger)index;

@end
