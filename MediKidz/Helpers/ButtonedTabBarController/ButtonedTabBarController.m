//
//  ButtonedTabBarController.m
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "ButtonedTabBarController.h"
#import "BasicNavigationController.h"
#import "ButtonedTab.h"

#define K_TAB_BAR_HEIGHT        65
#define K_ANIMATION_DURATION    0.3f

@interface ButtonedTabBarController ()

@property (nonatomic, strong, readwrite)     UIView                 *contentView;
@property (nonatomic, strong, readwrite)     UIViewController       *selectedViewController;

@property (nonatomic, strong)               NSArray                 *tabViewControllers;

@end

@implementation ButtonedTabBarController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tabBarView = [[ButtonedTabBarView alloc] initWithBackgroundImage:[UIImage imageNamed:@"tabBarBg"]];
    self.tabBarView.delegate = self;
    
    [self.view addSubview:self.contentView];
    [self.view addSubview:self.tabBarView];
    
    if (self.tabViewControllers.count)
    {
        [self _createTabs];
        self.selectedTabIndex = 0;
    }
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    CGRect tabBarRect = self.view.bounds;
    tabBarRect.origin.y = CGRectGetMaxY(tabBarRect) - K_TAB_BAR_HEIGHT;
    tabBarRect.size.height = K_TAB_BAR_HEIGHT;
    self.tabBarView.frame = tabBarRect;

    self.contentView.frame =  self.view.bounds;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (NSArray *)viewControllers
{
    return self.tabViewControllers;
}

- (void)setViewControllers:(NSArray *)viewControllers
{
    if (viewControllers != self.viewControllers)
    {
        self.tabViewControllers = viewControllers;
        
        if (self.isViewLoaded)
        {
            [self _createTabs];
            self.selectedTabIndex = 0;
        }
    }
}

- (BOOL)tabBarHidden
{
    return self.tabBarView.alpha == 0.0f;
}

- (void)setTabBarHidden:(BOOL)tabBarHidden
{
    self.tabBarView.alpha = tabBarHidden ? 0.0f : 1.0f;
    self.tabBarView.hidden = tabBarHidden;
    /*
    if (tabBarHidden)
    {
        [UIView animateWithDuration:K_ANIMATION_DURATION
                         animations:^
         {
             self.tabBarView.alpha = 0.0f;
         }
                         completion:^(BOOL finished)
         {
             self.tabBarView.hidden = YES;
         }];
    }
    else
    {
        self.tabBarView.hidden = NO;
        [UIView animateWithDuration:K_ANIMATION_DURATION
                         animations:^
         {
             self.tabBarView.alpha = 1.0f;
         }];
    }
    */
}

- (NSUInteger)selectedTabIndex
{
    return [self.viewControllers indexOfObject:self.selectedViewController];
}

- (void)setSelectedTabIndex:(NSUInteger)selectedTabIndex
{
    if (self.viewControllers.count > selectedTabIndex)
    {
        self.selectedViewController = self.viewControllers[selectedTabIndex];
    }
}

- (void)setSelectedViewController:(UIViewController *)selectedViewController
{
    UIViewController* presentedViewController = self.selectedViewController;
    
    if (presentedViewController != selectedViewController)
    {
        _selectedViewController = selectedViewController;
        
        if (!self.childViewControllers)
        {
            [presentedViewController viewWillDisappear:NO];
            [selectedViewController viewWillAppear:NO];
        }
        
        [presentedViewController willMoveToParentViewController:nil];
        [presentedViewController removeFromParentViewController];
        
        [selectedViewController willMoveToParentViewController:self];
        self.contentView = self.selectedViewController.view;
        self.title = self.selectedViewController.title;
        [self addChildViewController:selectedViewController];
        [selectedViewController didMoveToParentViewController:self];
        
        [presentedViewController didMoveToParentViewController:nil];
        
        if (!self.childViewControllers)
        {
            [presentedViewController viewDidDisappear:NO];
            [selectedViewController viewDidAppear:NO];
        }
        
        if ([presentedViewController isKindOfClass:[BasicNavigationController class]])
        {
            BasicNavigationController* navCtrl = (BasicNavigationController *)presentedViewController;
            [navCtrl popToRootViewControllerAnimated:NO];
        }
        
        self.tabBarView.selectedTabIndex = [self.viewControllers indexOfObject:selectedViewController];
    }
}

- (void)setContentView:(UIView *)contentView
{
    if (self.contentView != contentView)
    {
        [self.contentView removeFromSuperview];
        _contentView = contentView;
        [self.view insertSubview:self.contentView belowSubview:self.tabBarView];
        [self.view setNeedsLayout];
    }
}

#pragma mark - Private

- (void)_createTabs
{
    NSMutableArray *tabs = [NSMutableArray arrayWithCapacity:self.viewControllers.count];
    
    __weak typeof(self) weakSelf = self;
    [self.viewControllers enumerateObjectsUsingBlock:^(UIViewController *viewController, NSUInteger idx, BOOL *stop)
    {
        NSArray *viewControllersToUpdate = nil;
        
        if ([viewController isKindOfClass:UINavigationController.class])
        {
            UINavigationController *navCtrl = (UINavigationController *)viewController;
            viewControllersToUpdate = navCtrl.viewControllers;
            
            if ([viewController isKindOfClass:BasicNavigationController.class])
            {
                ((BasicNavigationController *)viewController).buttonedTabBar = weakSelf;
            }
        }
        else if (viewController.navigationController)
        {
            viewControllersToUpdate = viewController.navigationController.viewControllers;
            
            if ([viewController isKindOfClass:BasicNavigationController.class])
            {
                BasicNavigationController *navVC = (BasicNavigationController *)viewController;
                navVC.buttonedTabBar = weakSelf;
            }
        }
        else
        {
            viewControllersToUpdate = @[viewController];
        }
        
        [weakSelf _setTabBarToControllers:viewControllersToUpdate];

        UIImage* defaultImage = [UIImage imageNamed:@"tabBarIcon"];
        UIImage* defaultSelectedImage = [UIImage imageNamed:@"tabBarSelectedIcon"];
        
        UIImage* tabImage = viewController.tabBarItem.image ? viewController.tabBarItem.image : defaultImage;
        UIImage* tabImageHighlighted = viewController.tabBarItem.selectedImage ? viewController.tabBarItem.selectedImage : defaultSelectedImage;
        NSString* tabTitle = /*controller.tabBarItem.title ? controller.tabBarItem.title :*/ @"";
        [tabs addObject:[[ButtonedTab alloc] initWithImage:tabImage
                                          highlightedImage:tabImageHighlighted
                                                     title:tabTitle]];
    }];
    
    self.tabBarView.tabs = tabs;
}

- (void)_setTabBarToControllers:(NSArray *)viewControllers
{
    [viewControllers enumerateObjectsUsingBlock:^(id viewController, NSUInteger idx, BOOL *stop)
    {
        if ([viewController isKindOfClass:BasicViewController.class])
        {
            BasicViewController *basicVC = (BasicViewController *)viewController;
            __weak typeof(self) weakSelf = self;
            basicVC.buttonedTabBar = weakSelf;
        }
    }];
}

#pragma mark - ButtonedTabBarViewDelegate

- (BOOL)tabBar:(ButtonedTabBarView *)tabBar willSelectTabAtIndex:(NSUInteger)index
{
    ButtonedTab* tab = self.tabBarView.tabs[index];
    BOOL canOpenTab = tab.enabled;
    id selectedViewController = self.viewControllers[index];
    
    if ([selectedViewController isKindOfClass:BasicNavigationController.class])
    {
        BasicNavigationController *navCtrl = (BasicNavigationController*)selectedViewController;
        selectedViewController = (BasicViewController *)navCtrl.visibleViewController;
    }
    else
    {
        selectedViewController = selectedViewController;
    }
    
    if (canOpenTab && [selectedViewController respondsToSelector:@selector(tabBar:willSelectTabAtIndex:)])
    {
        canOpenTab = [selectedViewController tabBar:self.tabBarView willSelectTabAtIndex:index];
    }
    
    return canOpenTab;
}

- (void)tabBar:(ButtonedTabBarView *)tabBar didSelectTabAtIndex:(NSUInteger)index
{
    if (self.selectedTabIndex != index)
    {
        self.selectedTabIndex = index;
        
        BasicViewController *visibleVC = nil;
        if ([self.selectedViewController isKindOfClass:[BasicNavigationController class]])
        {
            BasicNavigationController *navCtrl = (BasicNavigationController *)self.selectedViewController;
            visibleVC = (BasicViewController *)navCtrl.visibleViewController;
        }
        else
        {
            visibleVC = (BasicViewController *)self.selectedViewController;
        }
        
        if ([visibleVC respondsToSelector:@selector(tabBar:didSelectTabAtIndex:)])
        {
            [visibleVC tabBar:self.tabBarView didSelectTabAtIndex:index];
        }
    }
    else
    {
        if ([self.selectedViewController isKindOfClass:[UINavigationController class]])
        {
            [(UINavigationController *)self.selectedViewController popToRootViewControllerAnimated:YES];
        }
    }
}

#pragma mark Camera And Microphone access

- (void)showCameraAccessAlert
{
	NSString* titleString = NSLocalizedString(@"Camera and Microphone permissions denied.  Please enable the permissions in the Settings to use.  Update now?", nil);
	UIAlertController* alertController = [UIAlertController alertControllerWithTitle:nil message:titleString preferredStyle:UIAlertControllerStyleAlert];
	
	UIAlertAction* settingsAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Settings", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
		NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
		[[UIApplication sharedApplication] openURL:url];
	}];
	
	UIAlertAction* notNowAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Not Now", nil) style:UIAlertActionStyleDefault handler:nil];
	
	[alertController addAction:settingsAction];
	[alertController addAction:notNowAction];
	[self presentViewController:alertController animated:YES completion:nil];
}

- (void) showCameraRestrictedAlert
{
	NSString* titleString = NSLocalizedString(@"The use of the camera is restricted and is necessary for Get It Hoops.  Please contact the device owner for access.", nil);
	UIAlertController* alertController = [UIAlertController alertControllerWithTitle:nil message:titleString preferredStyle:UIAlertControllerStyleAlert];
	
	UIAlertAction* okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok", nil) style:UIAlertActionStyleCancel handler:nil];
	
	[alertController addAction:okAction];
	[self presentViewController:alertController animated:YES completion:nil];
}

@end
