//
//  ButtonedTabBarController.h
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "BasicViewController.h"
#import "ButtonedTabBarView.h"

@interface ButtonedTabBarController : BasicViewController <ButtonedTabBarViewDelegate>

@property (nonatomic, strong)               NSArray*                viewControllers;
@property (nonatomic, strong, readonly)     UIViewController*       selectedViewController;
@property (nonatomic, assign)               NSUInteger              selectedTabIndex;

@property (nonatomic, strong, readonly)     UIView*                 contentView;

@property (nonatomic, strong)               ButtonedTabBarView*     tabBarView;
@property (nonatomic, assign)               BOOL                    tabBarHidden;

@end
