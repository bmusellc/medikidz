//
//  ButtonedTab.m
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "ButtonedTab.h"

@implementation ButtonedTab

- (instancetype)initWithImage:(UIImage*)image highlightedImage:(UIImage *)highlightedImage title:(NSString*)title
{
    self = [super init];
    if (self)
    {
        self.imageView.contentMode = UIViewContentModeCenter;
        self.adjustsImageWhenHighlighted = NO;
        [self setTitle:title forState:UIControlStateNormal];
        [self setImage:image forState:UIControlStateNormal];
        [self setImage:highlightedImage forState:UIControlStateSelected];
        [self setImage:highlightedImage forState:UIControlStateSelected | UIControlStateHighlighted];        
        self.clipsToBounds = NO;
        self.imageView.clipsToBounds = NO;
        self.exclusiveTouch = YES;
        
        [self sizeToFit];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (self.bounds.size.height > self.superview.bounds.size.height)
    {
        self.imageView.contentMode = UIViewContentModeBottom;
        self.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 6, 0);
    }
    else
    {
        self.imageView.contentMode = UIViewContentModeCenter;
        self.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    }
}

@end
