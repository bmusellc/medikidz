//
//  ErrorDefines.m
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "ErrorDefines.h"

@implementation NSError (UploadErrors)

+ (NSError *)defaultError
{
    NSDictionary *errorDict = [NSDictionary dictionaryWithObject:K_ERROR_OCCURED
                                                          forKey:NSLocalizedDescriptionKey];
    return [NSError errorWithDomain:NSURLErrorDomain
                               code:500
                           userInfo:errorDict];
}


+ (NSError *)cancelledByUserError
{
    NSDictionary *errorInfo = [NSDictionary dictionaryWithObject:K_USER_CANCELLED_TEXT
                                                          forKey:NSLocalizedDescriptionKey];
    return [NSError errorWithDomain:NSURLErrorDomain
                               code:401
                           userInfo:errorInfo];
}

@end