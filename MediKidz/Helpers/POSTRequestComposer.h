//
//  POSTRequestComposer.h
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface POSTRequestComposer : NSObject

- (instancetype)initWithEncoding:(NSStringEncoding)encoding;
- (void)addText:(NSString*)text forKey:(NSString*)key;
- (void)addImage:(NSString*)imagePath forKey:(NSString*)key;
- (NSData*)body;
+ (NSString*)boundary;

@end
