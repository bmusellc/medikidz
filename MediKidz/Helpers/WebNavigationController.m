//
//  WebNavigationController.m
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "WebNavigationController.h"
#import <WebKit/WebKit.h>

#define kToolbarHeight      44.0f
#define kButtonsMargin      6.0f
#define kAnimationDuration  0.3f

#define kLoadingPlaceholder @"LOADING…"

@interface WebNavigationController ()

@property (nonatomic, strong)   WKWebView           *webView;
@property (nonatomic, strong)   NSURL               *url;

@property (nonatomic, strong)   UIToolbar           *toolbar;
@property (nonatomic, strong)   UIBarButtonItem     *backButton;
@property (nonatomic, strong)   UIBarButtonItem     *forwardButton;
@property (nonatomic, strong)   UIBarButtonItem     *stopButton;
@property (nonatomic, strong)   UIBarButtonItem     *refreshButton;

@end

@interface WebNavigationController (WKNavigationDelegate) <WKNavigationDelegate> @end

@implementation WebNavigationController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self _addWebview];
    [self _addToolbar];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    CGRect toolbarRect = self.view.bounds;
    toolbarRect.origin.y = CGRectGetMaxY(toolbarRect) - kToolbarHeight;
    toolbarRect.size.height = kToolbarHeight;
    self.toolbar.frame = toolbarRect;
    
    CGRect webViewRect = self.view.bounds;
    self.webView.frame = webViewRect;
    
    UIEdgeInsets webViewInsets = self.webView.scrollView.contentInset;
    webViewInsets.bottom = kToolbarHeight;
    self.webView.scrollView.contentInset = webViewInsets;
}

- (void)openURL:(NSURL *)url
{
    self.url = url;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kAnimationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^
    {
        [self.webView loadRequest:[NSURLRequest requestWithURL:self.url]];
    });
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Private

- (void)_addWebview
{
    if (!self.webView)
    {
        WKWebViewConfiguration *wConf = [WKWebViewConfiguration new];
        self.webView = [[WKWebView alloc] initWithFrame:CGRectZero configuration:wConf];
        self.webView.navigationDelegate = self;
        [self.view addSubview:self.webView];
    }

}

- (void)_addToolbar
{
    if (!self.toolbar)
    {
        self.toolbar = [UIToolbar new];
        [self.view addSubview:self.toolbar];
        
        self.backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backIcon"]
                                                           style:UIBarButtonItemStylePlain
                                                          target:self
                                                          action:@selector(_backButtonTapped)];
        
        self.forwardButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"forwardIcon"]
                                                              style:UIBarButtonItemStylePlain
                                                             target:self
                                                             action:@selector(_forwardButtonTapped)];
        
        self.stopButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop
                                                                        target:self
                                                                        action:@selector(_stopButtonTapped)];
        
        self.refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
                                                                           target:self
                                                                           action:@selector(_refreshButtonTapped)];
        [self _updateNavButtons];
    }
}

- (void)_updateNavButtons
{
    self.backButton.enabled = self.webView.canGoBack;
    self.forwardButton.enabled = self.webView.canGoForward;
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedSpace.width = kButtonsMargin;
    
    [UIView animateWithDuration:kAnimationDuration
                     animations:^
    {
        self.toolbar.items = @[self.backButton,
                               fixedSpace,
                               self.forwardButton,
                               flexibleSpace,
                               self.webView.isLoading ? self.stopButton : self.refreshButton];
    }];

}

- (void)_backButtonTapped
{
    [self.webView goBack];
}

- (void)_forwardButtonTapped
{
    [self.webView goForward];
}

- (void)_stopButtonTapped
{
    [self.webView stopLoading];
}

- (void)_refreshButtonTapped
{
    [self.webView reload];
}

@end

@implementation WebNavigationController (WKNavigationDelegate)

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation
{
    self.title = kLoadingPlaceholder;
    [self _updateNavButtons];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
//    [self.webView evaluateJavaScript:@"window.scroll(0,0)" completionHandler:nil];
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.15 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^
//    {
//        self.webView.scrollView.contentOffset = CGPointMake(0, -(self.webView.scrollView.contentInset.top));
//    });
    self.title = [webView.title uppercaseString];
    [self _updateNavButtons];
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    self.title = [webView.title uppercaseString];
    [self _updateNavButtons];
}

@end
