//
//  TabBar+HomeScreenControllers.m
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "TabBar+HomeScreenControllers.h"
#import "VisitsViewController.h"
#import "PointsViewController.h"
#import "BasicNavigationController.h"
#import "ComicsTabbedViewController.h"
#import "SettingsViewController.h"

@implementation ButtonedTabBarController (HomeScreenControllers)

+ (NSArray*)homeScreenControllers
{
    VisitsViewController *visitsVC = [VisitsViewController new];
    PointsViewController *pointsVC = [PointsViewController new];
    ComicsTabbedViewController *comicsVC = [ComicsTabbedViewController new];
    SettingsViewController *settingsVC = [SettingsViewController new];

	NSArray* controllers = @[visitsVC, pointsVC, comicsVC, settingsVC];
	
    return controllers;
}

@end
