//
//  StorageDefines.h
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import <Foundation/Foundation.h>

#define K_DOCUMENTS_DIR                 [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]
#define K_CACHES_DIR                    [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject]
#define kTempDir                        NSTemporaryDirectory()

#define kDownloadedFilesDirPath         [K_DOCUMENTS_DIR stringByAppendingPathComponent:@"DownloadedFiles"]
#define K_METADATA_ROOT_PATH            [K_CACHES_DIR stringByAppendingPathComponent:@"VideoMetadata"]
#define K_TIMER_GAME_RECORDS            [K_DOCUMENTS_DIR stringByAppendingPathComponent:@"timerGameRecords.plist"]

#define kCraveDataPath                  [K_DOCUMENTS_DIR stringByAppendingPathComponent:@"CraveData"]
#define kBooksDataPath                  [kCraveDataPath stringByAppendingPathComponent:@"Books"]
#define kAuthorsDataPath                [kCraveDataPath stringByAppendingPathComponent:@"Authors"]
#define kNotificationsDataPath          [kCraveDataPath stringByAppendingPathComponent:@"Notifications"]
#define kNotificationSendersDataPath    [kCraveDataPath stringByAppendingPathComponent:@"NotificationSenders"]

#define kTemporaryDownloadedBooks       [kTempDir stringByAppendingPathComponent:@"Books"]
#define kTemporaryDownloadedChapters    [kTempDir stringByAppendingPathComponent:@"Chapters"]
