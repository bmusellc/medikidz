//
//  POSTRequestComposer.m
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "POSTRequestComposer.h"

#define K_DEFAULT_BOUNDARY  @"POSTBoundary"

@interface POSTRequestComposer ()

@property (nonatomic, assign) NSStringEncoding  encoding;
@property (nonatomic, strong) NSMutableData*    bodyData;
@property (nonatomic, strong) NSString*         boundary;

@end

@implementation POSTRequestComposer

- (instancetype)init
{
    return [self initWithEncoding:NSUTF8StringEncoding];
}

- (instancetype)initWithEncoding:(NSStringEncoding)encoding
{
    self = [super init];
    if (self)
    {
        self.encoding = encoding ? encoding : NSUTF8StringEncoding;
        self.bodyData = [NSMutableData data];
        self.boundary = K_DEFAULT_BOUNDARY;
    }
    return self;
}

- (void)addText:(NSString*)text forKey:(NSString*)key
{
    if (!text || !key || text.length == 0 || key.length == 0)
    {
        return;
    }
    
    [self.bodyData appendData:[self _dataFromStringWithFormat:@"--%@\r\n", self.boundary]];
    [self.bodyData appendData:[self _dataFromStringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key]];
    [self.bodyData appendData:[self _dataFromStringWithFormat:@"%@\r\n", text]];
}

- (void)addImage:(NSString*)imagePath forKey:(NSString*)key
{
    if (!imagePath || !key || imagePath.length == 0 || key.length == 0)
    {
        return;
    }
    
    NSData* imageData = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:imagePath]];
    NSString* fileName = [imagePath lastPathComponent];
    NSString* fileExt = [[imagePath pathExtension] lowercaseString];
    NSString* contentType = nil;
    
    if ([fileExt isEqualToString:@"png"])
    {
        contentType = @"Content-Type: image/png\r\n\r\n";
    }
    else if ([fileExt isEqualToString:@"jpg"] || [fileExt isEqualToString:@"jpeg"])
    {
        contentType = @"Content-Type: image/jpeg\r\n\r\n";
    }
    
    if (imageData)
    {
        [self.bodyData appendData:[self _dataFromStringWithFormat:@"--%@\r\n", self.boundary]];
        [self.bodyData appendData:[self _dataFromStringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", key, fileName]];
        [self.bodyData appendData:[self _dataFromStringWithFormat:contentType]];
        [self.bodyData appendData:imageData];
        [self.bodyData appendData:[self _dataFromStringWithFormat:@"\r\n"]];
    }
}

- (NSData *)body
{
    NSMutableData* body = [self.bodyData mutableCopy];
    [body appendData:[self _dataFromStringWithFormat:@"--%@--\r\n\0", self.boundary]];
    
    return body;
}

+ (NSString *)boundary
{
    return K_DEFAULT_BOUNDARY;
}

#pragma mark - Private

- (NSData*)_dataFromStringWithFormat:(NSString *)format, ...
{
    NSData* data = nil;

    if (format)
    {
        va_list argList;
        va_start(argList, format);
        
        NSString* str = [[NSString alloc] initWithFormat:format arguments:argList];
        data = [str dataUsingEncoding:self.encoding];
        
        va_end(argList);

    }
    
    return data;
}

@end
