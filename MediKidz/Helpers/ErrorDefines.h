//
//  ErrorDefines.h
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import <UIKit/UIKit.h>

#define K_ERROR_OCCURED                    NSLocalizedString(@"Oops, something isn't right. It's us, not you. Please try again later.", @"en_EN")
#define K_USER_CANCELLED_TEXT              NSLocalizedString(@"Error occured.\nPlease try again.", @"en_EN")
#define K_USER_CANCELLED_LOGIN_TITLE       NSLocalizedString(@"Permission for Crave is disabled", @"en_EN")
#define K_USER_CANCELLED_LOGIN_TEXT        NSLocalizedString(@"Please go to the Settings > Facebook and allow Crave to use your Facebook account.", @"en_EN")
#define K_INVALID_FB_SESSION_TEXT          NSLocalizedString(@"Your current session is no longer valid. Please log in again.", @"en_EN")
#define K_OTHER_ERROR_WITHOUT_DESCRIPTION  NSLocalizedString(@"Please retry.\n\nIf the problem persists contact us.", @"en_EN")
#define K_OTHER_ERROR_WITH_DESCRIPTION     NSLocalizedString(@"Please retry.\n\nIf the problem persists contact us and mention this error code: ", @"en_EN")
#define K_NETWORK_CONNECTION_ERROR         NSLocalizedString(@"The internet connection appears to be offline.", @"en_EN")

@interface NSError (UploadErrors)

+ (NSError *)defaultError;
+ (NSError *)cancelledByUserError;

@end
