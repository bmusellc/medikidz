//
//  JSONDataRequest.m
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "JSONDataRequest.h"
#import "ErrorDefines.h"

@implementation JSONDataRequest

- (id)prepareReceivedData:(id)data
{
    id dataToSend = nil;
    
    NSError *jsonParsingError = nil;
    NSDictionary *parsedJSONResponseData = [NSJSONSerialization JSONObjectWithData:data
                                                                           options:0
                                                                             error:&jsonParsingError];
    if (jsonParsingError)
    {
        dataToSend = jsonParsingError;
    }
    else
    {
        NSError* responseError = [self _checkForErrorsInResponse:parsedJSONResponseData];
        dataToSend = responseError ? responseError : parsedJSONResponseData;
    }
    
    return dataToSend;
}

#pragma mark - Private

- (NSError *)_checkForErrorsInResponse:(NSDictionary *)response
{
    NSError* error = [NSError defaultError];
    
    NSString* status = response[@"status"];
    if ([status isEqualToString:@"success"])
    {
        // everything is fine, so don't need to return errors
        error = nil;
    }
    else if ([status isEqualToString:@"fail"] || [status isEqualToString:@"error"])
    {
        NSString* code = response[@"data"][@"code"];
        NSString* errorMsg = nil;
        id errorData = response[@"data"][@"message"] ? response[@"data"][@"message"] : response[@"data"];
        if ([errorData isKindOfClass:[NSDictionary class]])
        {
            NSDictionary* errorInfo = errorData;
            errorMsg = [errorData valueForKey:[errorInfo allKeys][0]];
        }
        else
        {
            errorMsg = errorData;
        }
        
        NSDictionary* errorDict = [NSDictionary dictionaryWithObject:errorMsg forKey:NSLocalizedDescriptionKey];
        NSError* responseError = [NSError errorWithDomain:NSURLErrorDomain
                                                     code:code.integerValue ? code.integerValue : error.code userInfo:errorDict];
        DLog(@"Error: %@", responseError);
        // use 'responseError' if you want to inform user what exactly happened
        error = responseError;
    }
    else
    {
        DLog(@"Wrong response format: %@", response);
    }
    
    return error;
}

@end
