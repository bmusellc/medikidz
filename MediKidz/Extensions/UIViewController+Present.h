//
//  UIViewController+Present.h
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Present)

- (void)presentOverRootViewControllerAnimated:(BOOL)animated completion:(void (^)(void))completion;

@end
