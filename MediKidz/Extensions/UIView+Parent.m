//
//  UIView+Parent.m
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "UIView+Parent.h"

@implementation UIView (Parent)

- (id)parentViewController
{
    id nextResponder = [self nextResponder];
    
    if ([nextResponder isKindOfClass:UIViewController.class])
    {
        return nextResponder;
    }
    else if ([nextResponder isKindOfClass:UIView.class])
    {
        return [nextResponder parentViewController];
    }
    else
    {
        return nil;
    }
}

@end
