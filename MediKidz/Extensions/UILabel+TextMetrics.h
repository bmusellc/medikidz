//
//  UILabel+TextMetrics.h
//  GetItHoops
//
//  Created by Alexey Smirnov on 5/15/15.
//  Copyright (c) 2015 bMuse. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (TextMetrics)

- (void)adjustsFont:(UIFont *)font toFitIntoSizeConstraints:(CGSize)sizeConstraints;
- (CGSize)sizeThatReallyFits:(CGSize)size;

@end
