//
//  UILabel+TextMetrics.m
//  GetItHoops
//
//  Created by Alexey Smirnov on 5/15/15.
//  Copyright (c) 2015 bMuse. All rights reserved.
//

#import "UILabel+TextMetrics.h"

@implementation UILabel (TextMetrics)

- (void)adjustsFont:(UIFont *)font toFitIntoSizeConstraints:(CGSize)sizeConstraints
{
    NSString* fontName = font.fontName;
    CGFloat fontSize = font.pointSize;
    CGSize textSize = [self.text boundingRectWithSize:CGSizeMake(sizeConstraints.width, CGFLOAT_MAX)
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{NSFontAttributeName:font}
                                              context:nil].size;
    textSize.width = ceilf(textSize.width);
    textSize.height = ceilf(textSize.height);
    while (textSize.width > sizeConstraints.width || textSize.height > sizeConstraints.height)
    {
        @autoreleasepool
        {
            --fontSize;
            self.font = [UIFont fontWithName:fontName size:fontSize];
            textSize = [self.text boundingRectWithSize:CGSizeMake(sizeConstraints.width, CGFLOAT_MAX)
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:@{NSFontAttributeName:self.font}
                                               context:nil].size;
            textSize.width = ceilf(textSize.width);
            textSize.height = ceilf(textSize.height);
        }
    }
}

- (CGSize)sizeThatReallyFits:(CGSize)size
{
    CGSize calculatedLabelSize = [self sizeThatFits:size];
//    int numLines = (int)(self.bounds.size.height / self.font.leading);
    calculatedLabelSize.height += (self.font.lineHeight - self.font.pointSize) /* * numLines*/;
    
    return calculatedLabelSize;

}

@end
