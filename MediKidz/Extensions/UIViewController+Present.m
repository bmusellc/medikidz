//
//  UIViewController+Present.m
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "UIViewController+Present.h"

@implementation UIViewController (Present)

- (void)presentOverRootViewControllerAnimated:(BOOL)animated completion:(void (^)(void))completion
{
    UIWindow *appWindow = [[[UIApplication sharedApplication] delegate] window];
    [appWindow.rootViewController presentViewController:self animated:animated completion:completion];
}

@end
