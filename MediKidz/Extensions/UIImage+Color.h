//
//  UIImage+Color.h
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage(UIImage)

+ (UIImage *)imageWithColor:(UIColor *)color;

@end
