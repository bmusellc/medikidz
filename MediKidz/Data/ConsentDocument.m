//
//  ConsentDocument.m
//  MediKidz
//
//  Created by Alexey Smirnov on 6/24/16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "ConsentDocument.h"

@interface ConsentDocument ()

@property (strong, nonatomic)   NSArray<NSString *> *ipsum;
@property (strong, nonatomic)   NSArray<NSString *> *sums;

@end

@implementation ConsentDocument

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.ipsum = @[
                       @"This screen will provide more information about which data is collected and how it is used.",
                       @"This screen will provide more information about patient and parent privacy.",
                       @"This screen will provide more information about how data is used.",
                       @"This screen will provide more information about the time commitment required from patients and their parents.",
                       @"This screen will provide more information about the survey questions asked during the study.",
                       @"This screen will provide more information about the types of activities we will offer your child during the study.",
                       @"This screen will provide more information about the notifications we will send to your child during the study.",
                       @"We will not collect or store any new data if you choose to withdraw. To withdraw from the study, simply select \"Leave Study\" on the Profile tab."
                       ];
        self.sums = @[
                      @"Throughout this study the app will collect information from your iPhone to help us send you and your child relevant material, to remind you about future appointments and to let your child track their progress on various games and activities.",
                      @"We take your privacy and the privacy of your child very serisouly. We will replace all personal identifiable information with a random code. The coded data will be encrypted and stored on a secure cloud server under the control of Sanofi to prevent improper access.",
                      @"Your coded study data will be used for research by the Sanofi Group.",
                      @"This study takes place place over the course of 52 weeks and will require a visit to the doctor's office or to a lab once every two weeks.",
                      @"We will ask you to answer brief surveys about how you and your child are feeling during the study period.",
                      @"During the study we will provide your child with fun activities to keep them motivated and engaged in the study and help them understand it better.",
                      @"We will send you SMS and in-app notifications to remind you of upcoming visits and let you reschedule appointments if needed.",
                      @"You may withdraw your consent and discontinue participation at any time."
                      ];
        
        self.title = @"Research Health Study Consent Form";
        
        NSArray<NSNumber *> *sectionTypes = @[@(ORKConsentSectionTypeDataGathering),
                                              @(ORKConsentSectionTypePrivacy),
                                              @(ORKConsentSectionTypeDataUse),
                                              @(ORKConsentSectionTypeTimeCommitment),
                                              @(ORKConsentSectionTypeStudySurvey),
                                              @(ORKConsentSectionTypeStudyTasks),
                                              @(ORKConsentSectionTypeCustom),
                                              @(ORKConsentSectionTypeWithdrawing)];
        
        NSMutableArray<ORKConsentSection *> *sections = [NSMutableArray array];
        
        [sectionTypes enumerateObjectsUsingBlock:^(NSNumber * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop)
        {
            ORKConsentSectionType sectionType = [obj integerValue];
            NSString *localizedIpsum = NSLocalizedString(self.ipsum[idx], @"");
            NSString *localizedSummary = NSLocalizedString(self.sums[idx], @"");
            ORKConsentSection *section = [[ORKConsentSection alloc] initWithType:sectionType];
            section.summary = localizedSummary;
            section.content = localizedIpsum;
            if (sectionType == ORKConsentSectionTypeCustom) // Notifications
            {
                section.customLearnMoreButtonTitle = @"Learn more about notifications";
                section.customImage = [UIImage imageNamed:@"consentNotification"];
                section.title = @"Notifications";
            }
            [sections addObject:section];
        }];
        
        self.sections = sections;
        
        ORKConsentSignature *signature = [ORKConsentSignature signatureForPersonWithTitle:nil
                                                                         dateFormatString:nil
                                                                               identifier:@"ConsentDocumentParticipantSignature"];
        [self addSignature:signature];
    }
    return self;
}
@end
