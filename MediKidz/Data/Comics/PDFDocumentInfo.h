//
//  PDFDocumentInfo.h
//  PDFPreview
//
//  Created by Stanislav Kovalchuk on 6/15/16.
//  Copyright © 2016 Stanislav Kovalchuk. All rights reserved.
//

#import <Foundation/Foundation.h>

@import CoreGraphics;

@interface PDFDocumentInfo : NSObject

@property (nonatomic, readonly) NSURL*              url;
@property (nonatomic, readonly) NSInteger           numberOfPages;
@property (nonatomic, readonly) CGPDFDocumentRef    pdfDocument;
@property (nonatomic, readonly) NSArray*            frames;
@property (nonatomic, readonly) CGSize             pageSize;

- (instancetype)initWithURL:(NSURL *)url;

@end
