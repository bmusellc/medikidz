//
//  ConsentDocument.h
//  MediKidz
//
//  Created by Alexey Smirnov on 6/24/16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import <ResearchKit/ResearchKit.h>

@interface ConsentDocument : ORKConsentDocument

@property (readonly, nonatomic) NSArray<NSString *> *ipsum;
@property (readonly, nonatomic) NSArray<NSString *> *sums;

@end
