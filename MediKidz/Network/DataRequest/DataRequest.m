//
//  DataRequest.m
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "DataRequest.h"

#define kDefaultBoundary        @"POSTBoundary"
#define kNoDataErrorTitle       @"No data received"

@interface DataRequest ()

@property (nonatomic, strong)   NSURL                       *url;

@property (nonatomic, assign)   BOOL                        isExecuting;
@property (nonatomic, copy)     void                        (^completion)(id, NSError *);

@property (nonatomic, strong)   NSURLSession                *session;
@property (nonatomic, strong)   NSURLSessionDataTask        *dataTask;
@property (nonatomic, strong)   NSMutableURLRequest         *request;

@end

@implementation DataRequest

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.isExecuting = NO;
        self.boundary = kDefaultBoundary;
    }
    return self;
}

- (instancetype)initWithURL:(NSURL *)requestURL
{
    NSAssert(requestURL, @"Could not init request without URL");
    
    self = [super init];
    if (self)
    {
        self.isExecuting = NO;
        self.boundary = kDefaultBoundary;
        self.url = requestURL;
    }
    return self;
}

- (void)dealloc
{
    [self cancel];
}

- (void)setBody:(NSData *)body
{
    NSAssert(!self.isExecuting, @"Could not change body data when request already executing");
    
    _body = body;
}

- (void)setBoundary:(NSString *)boundary
{
    NSAssert(!self.isExecuting, @"Could not change boundary when request already executing");
    
    _boundary = boundary;
}

- (void)startWithCompletion:(void (^)(id response, NSError *error))completion
{
    NSAssert(self.url, @"Could not start request without url");
    
    if (self.isExecuting) return;
    
    self.isExecuting = YES;
    self.completion = completion;
    typeof(self) __weak weakSelf = self;
    
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    self.session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
    self.request = [self _requestWithBodyData:self.body andBoundary:self.boundary];
    
    self.dataTask = [self.session dataTaskWithRequest:self.request
                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        if (error)
        {
            DLog(@"Request Error: %@", error.localizedDescription);
            if (weakSelf.completion) weakSelf.completion(nil, error);
        }
        else
        {
            if (weakSelf.dataTask.state == NSURLSessionTaskStateCompleted)
            {
                if (data.length)
                {
                    id response = [weakSelf prepareReceivedData:data];
                    if ([response isKindOfClass:NSError.class])
                    {
                        if (weakSelf.completion) weakSelf.completion(nil, response);
                    }
                    else
                    {
                        if (weakSelf.completion) weakSelf.completion(response, nil);
                    }
                }
                else
                {
                    NSDictionary *errorDict = [NSDictionary dictionaryWithObject:kNoDataErrorTitle
                                                                          forKey:NSLocalizedDescriptionKey];
                    NSError *noDataError = [NSError errorWithDomain:NSURLErrorDomain
                                                               code:500
                                                           userInfo:errorDict];
                    if (weakSelf.completion) weakSelf.completion(nil, noDataError);
                }
                
            }
            weakSelf.dataTask = nil;
            weakSelf.session = nil;
            
            // need to use at least one 'self' just to retain object while block is executing
            self.isExecuting = NO;
        }
    }];
    
    [self.dataTask resume];
}

- (id)prepareReceivedData:(id)data
{    
    return data;
}

- (void)cancel
{
    self.isExecuting = NO;
    [self.session invalidateAndCancel];
    self.session = nil;
    [self.dataTask cancel];
    self.dataTask = nil;
    self.request = nil;
}

#pragma mark - Private

- (NSMutableURLRequest *)_requestWithBodyData:(NSData *)bodyData andBoundary:(NSString *)boundary
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:self.url];
    
    request.HTTPMethod = bodyData.length ? @"POST" : @"GET";
    if (bodyData.length)
    {
        request.HTTPBody = bodyData;
        
        NSString *contentLength = [NSString stringWithFormat:@"%lu", (unsigned long)bodyData.length];
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; charset=utf-8; boundary=%@", boundary];
        [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
        [request addValue:contentLength forHTTPHeaderField:@"Content-Length"];
    }
    
    return request;
}

@end
