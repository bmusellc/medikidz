//
//  DataRequest.h
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DataRequest : NSObject

@property (nonatomic, readonly)     NSURL       *url;
@property (nonatomic, strong)       NSData      *body;
@property (nonatomic, strong)       NSString    *boundary;

- (instancetype)initWithURL:(NSURL *)requestURL;
- (void)startWithCompletion:(void (^)(id response, NSError *error))completion;
- (void)cancel;

/*
 * Convinient method for preparing data structure for further actions
 * Subclasses could override this method to perform any data modifications.
 * After this modified data will be returned in completion handler.
 * Subclasses can also return NSError object if they don't receive expected values.
 */
- (id)prepareReceivedData:(id)data;

@end
