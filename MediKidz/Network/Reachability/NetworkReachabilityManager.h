//
//  NetworkReachabilityManager.h
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *kNetworkReachabilityLostNotification;
extern NSString *kNetworkReachabilityRestoredNotification;

#define PMNetworkReachabilityLostNotification       @"PMNetworkReachabilityLostConnectionNotification"
#define PMNetworkReachabilityRestoredNotification   @"PMNetworkReachabilityRestoredNotification"

@interface NetworkReachabilityManager : NSObject
{
	NSTimer *_reachabilityWaitTimer;
    NSTimer *_monitoringTimer;
    BOOL _latestCheckWasReachable;
}

+ (NetworkReachabilityManager *)shared;

//use it to quick check of the network reachability
+ (BOOL)isConnectedToNetwork;

//manages timer, which periodically checks for network reachability and posts notifications on
//state changes
- (void)beginMonitoring;
- (void)finishMonitoring;

//use it to check reacability and start pending timer that will continously check connection and will 
//send Notification in case of restoring
- (BOOL)checkNetworkReachability;

@end

@interface NetworkReachabilityManager (NoInternetAlertView)

- (void)showNetworkInaccessibleAlertView;

@end