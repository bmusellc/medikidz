//
//  NetworkReachabilityManager.m
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import "NetworkReachabilityManager.h"
#import "Reachability.h"
#import <SystemConfiguration/SCNetworkReachability.h>
#include <netinet/in.h>
#import "UIViewController+Present.h"

NSString *kNetworkReachabilityLostNotification = @"NetworkReachabilityLostConnectionNotification";
NSString *kNetworkReachabilityRestoredNotification = @"NetworkReachabilityRestoredNotification";

#define K_REACHABILITY_TIMER_INTERVAL           3.0f
#define REACHABILITY_HAS_NO_INTERNET_TITLE      @"No Internet connection detected.\nPlease connect to the Internet and try again."

static NetworkReachabilityManager *sharedInstance = nil;

@interface NetworkReachabilityManager(Private)

- (void)_startPendingTimer;
- (void)_stopPendingTimer;

@end

@implementation NetworkReachabilityManager

+ (NetworkReachabilityManager *)shared
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
    {
        sharedInstance = [NetworkReachabilityManager new];
    });
	
	return sharedInstance;
}

- (id)init 
{
    self = [super init];
    if (self) 
	{
        
    }
    return self;
}

- (void)dealloc
{
    [_monitoringTimer invalidate];
    [_reachabilityWaitTimer invalidate];
}

#pragma mark - Public methods implementation
+ (BOOL)isConnectedToNetwork
{
    // Create zero addy
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
	
    // Recover reachability flags
    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
    SCNetworkReachabilityFlags flags;
	
    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    CFRelease(defaultRouteReachability);
	
    if (!didRetrieveFlags)
    {
        printf("Error. Could not recover network reachability flags\n");
        return NO;
    }
    
    if (![[Reachability reachabilityForInternetConnection] isReachable])
    {
        return NO;
    }
	
    BOOL isReachable = flags & kSCNetworkFlagsReachable;
    BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
    return (isReachable && !needsConnection) ? YES : NO;
}

- (BOOL)checkNetworkReachability
{
	BOOL isConnected = [NetworkReachabilityManager isConnectedToNetwork];
	
	if (!isConnected)
	{
		[self _startPendingTimer];
	}
	return isConnected;
}

- (void)beginMonitoring
{
    if(!_monitoringTimer)
    {
        _latestCheckWasReachable = [NetworkReachabilityManager isConnectedToNetwork];
        
        _monitoringTimer = [NSTimer scheduledTimerWithTimeInterval:K_REACHABILITY_TIMER_INTERVAL 
                                                            target:self 
                                                          selector:@selector(_onTimer:)
                                                          userInfo:nil 
                                                           repeats:YES];
    }
}

- (void)finishMonitoring
{
    if(_monitoringTimer)
    {
        [_monitoringTimer invalidate];
        _monitoringTimer = nil;
    }
}

#pragma mark - Private methods implementation
- (void)_startPendingTimer
{
	[self _stopPendingTimer];
	
	_reachabilityWaitTimer = [NSTimer scheduledTimerWithTimeInterval:5.0
															  target:self selector:@selector(_onTimer:)
															userInfo:nil
															 repeats:NO];
}

- (void)_stopPendingTimer
{
	if(_reachabilityWaitTimer)
	{
		[_reachabilityWaitTimer invalidate];
		_reachabilityWaitTimer = NULL;
	}
}

- (void)_onTimer:(NSTimer *)timer
{
    if (timer == _reachabilityWaitTimer)
    {
        //for [self checkNetworkReachability]
        if ([NetworkReachabilityManager isConnectedToNetwork])
        {
			[self _stopPendingTimer];
            [[NSNotificationCenter defaultCenter]postNotificationName:kNetworkReachabilityRestoredNotification
                                                               object:nil];
        }
        else
        {
            [self _startPendingTimer];
        }
    }
    else if(timer == _monitoringTimer)
    {
        //for [self beginMonitoring]
        BOOL isNowReachable = [NetworkReachabilityManager isConnectedToNetwork];
        
        if(isNowReachable != _latestCheckWasReachable)
        {
            _latestCheckWasReachable = isNowReachable;
            NSString *notificationName = (isNowReachable) ? kNetworkReachabilityRestoredNotification : kNetworkReachabilityLostNotification;
            [[NSNotificationCenter defaultCenter] postNotificationName:notificationName
                                                                object:self];
        }
    }
	
}

@end

@implementation NetworkReachabilityManager (NoInternetAlertView)

- (void)showNetworkInaccessibleAlertView
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:REACHABILITY_HAS_NO_INTERNET_TITLE
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* closeAction = [UIAlertAction actionWithTitle:@"OK"
                                                          style:UIAlertActionStyleCancel
                                                        handler:nil];
    [alert addAction:closeAction];
    
    [alert presentOverRootViewControllerAnimated:YES completion:nil];}

@end