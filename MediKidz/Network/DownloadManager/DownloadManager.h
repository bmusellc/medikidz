//
//  DownloadManager.h
//  MediKidz
//
//  Created by Mike Ponomaryov on 22.06.16.
//  Copyright © 2016 bMuse Ukraine. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *DMDidStartDownloadingNotification;
extern NSString *DMDidFinishDownloadingNotification;
extern NSString *DMDownloadingProgressNotification;
extern NSString *DMDidCompleteWithErrorNotification;

extern NSString *DMSourceKey;
extern NSString *DMIdentifierKey;
extern NSString *DMProgressKey;
extern NSString *DMLocationKey;
extern NSString *DMSuggestedFileNameKey;
extern NSString *DMDownloadErrorKey;

typedef enum
{
    DownloadManagerTaskPriorityLow = 0,
    DownloadManagerTaskPriorityHigh
}
DownloadManagerTaskPriority;

@interface DownloadManager : NSObject

+ (instancetype)shared;

@property (nonatomic, readonly)   NSArray     *highPriorityTasks;
@property (nonatomic, readonly)   NSArray     *lowPriorityTasks;

- (NSUInteger)downloadURL:(NSURL*)url; // uses high priority by default
- (NSUInteger)downloadURL:(NSURL*)url priority:(DownloadManagerTaskPriority)priority;
- (void)cancelTaskWithID:(NSUInteger)taskID;
- (void)cancelAllTasks;
- (BOOL)isDownloadTaskExists:(NSUInteger)taskID;

@end
